/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;

import com.hybridearth.coordinaterecorder.utils.Log;
import com.hybridearth.coordinaterecorder.utils.MapMakerLocationManager;

/**
 * Singleton class to centralize certain information.
 * 
 * @author sciarcia
 * 
 */
public class MapMakerApplication extends Application {

    private static final String        TAG       = "MapMakerApplication";

    /** This instance of the application */
    private static MapMakerApplication instance  = null;

    /** A layout inflater to use throughout the application */
    private static LayoutInflater      mInflater = null;
    /** Shared preferences to store and read throughout the application */
    private static SharedPreferences   mSharedPref;

    /** 
     * to control in what state we're in:
     * <ul>
     * <li>EDIT: we're logged in and we're editing the map</li>
     * <li>LOGGED_IN: we're logged in and we're only looking at the map</li>
     * <li>LOGGED_OUT: we're not logged in, hence we probably can but log in</li>
     * </ul>
     * 
     */
    public enum AppState {
        /** we're logged in and we're editing the map */
        EDIT,
        /** we're logged in and we're only looking at the map */
        LOGGED_IN,
        /** we're not logged in, hence we probably can but log in */
        LOGGED_OUT
    }

    private static AppState                mAppState;

    /** Counter to track how many threads we've spawned */
    private int                            downloaders = 0;

    /** To check if we're connected to the internet */
    private ConnectivityManager            mCm;
    /** To geolocalize the device */
    private static MapMakerLocationManager mMMLocationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mSharedPref = getSharedPreferences(Consts.PREF_FILE, Context.MODE_PRIVATE);
        if (mSharedPref.getBoolean(Consts.PREF_LOGGED_IN, false)) {
            setAppState(AppState.LOGGED_IN);
        } else {
            setAppState(AppState.LOGGED_OUT);
        }
        mCm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mMMLocationManager = new MapMakerLocationManager(this);
    }

    /**
     * Call this when you start doing network stuff
     */
    public synchronized void threadStart() {
        downloaders++;
        Log.d(TAG, "Added thread. There are now " + downloaders + " active");
    }

    /**
     * Call this when your thread is done doing network stuff
     */
    public synchronized void threadStop() {
        downloaders--;
        Log.d(TAG, "Stopped thread. There are now " + downloaders + " active");
    }

    /**
     * This will tell you if there still are some threads around doing network stuff
     */
    public synchronized boolean isDownloading() {
        return downloaders != 0;
    }

    /**
     * 
     * @return an instance of the Application object
     */
    public static MapMakerApplication getInstance() {
        if (instance == null) {
            instance = new MapMakerApplication();
        }
        return instance;
    }

    /**
     * @return the Application context
     */
    public static Context getAppContext() {
        return getInstance().getApplicationContext();
    }

    /**
     * @return an instance of the LayoutInflater
     */
    public static LayoutInflater getInflater() {
        if (mInflater == null) {
            mInflater = (LayoutInflater) getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return mInflater;
    }

    /**
     * Once you call this you can .edit().putXXX().commit() your preferences,
     * or you can retrieve their values with .getXXX()
     * 
     * @return an object to access the Shared preferences
     */
    public static SharedPreferences getPreferences() {
        return mSharedPref;
    }

    /**
     * See {@link AppState}
     * 
     * @return the state the app is in
     */
    public static AppState getAppState() {
        return mAppState;
    }

    /**
     * See {@link AppState}
     * @param appState the state the app is in.
     */
    public static void setAppState(AppState appState) {
        mAppState = appState;
        mSharedPref.edit().putBoolean(Consts.PREF_LOGGED_IN, mAppState == AppState.LOGGED_OUT ? false : true).commit();
    }

    /**
     * Checks if we're connected to the internet
     * 
     * @return whether the device is connected to a data network (wifi or cellular data)
     */
    public boolean isConnected() {
        NetworkInfo activeNetwork = mCm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * 
     * @return an instance of the LocationManager
     */
    public static MapMakerLocationManager getLocationManager() {
        return mMMLocationManager;
    }

}
