/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.server;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.utils.Log;
import com.hybridearth.coordinaterecorder.utils.Utils.Task;

/**
 * Takes care of performing Login, Signup, Logout operations
 * 
 * @author sciarcia
 * 
 */
public class RegistrationManager extends AsyncTask<Task, Void, Void> {

    private static final String TAG = "RegistrationManager";

    /**
     * Interface for callback methods on operations
     * 
     * @author sciarcia
     *
     */
    public interface OnRegistrationListener {
        /**
         * method called when a Login operation concluded successfully
         * 
         * @param userId the userToken received from the server representing this user
         */
        void onLoginSuccessful(String userId);

        /**
         * method called when a Login operation concluded abnormally
         * 
         * @param error a string from the server indicating the error that cause this
         */
        void onLoginFailed(String error);

        /**
         * method called when a Logout operation concluded successfully
         * 
         * @param userId the userToken received from the server representing the user who just logged out
         */
        void onLogoutSuccessful(String userId);

        /**
         * method called when a Logout operation concluded abnormally
         * 
         * @param error a string from the server indicating the error that cause this
         */
        void onLogoutFailed(String error);

        /**
         * method called when a Signup operation concluded successfully
         * 
         * @param userId the userToken received from the server representing the user
         */
        void onSignupSuccessful(String userId);

        /**
         * method called when a Signup operation concluded abnormally
         * 
         * @param error a string from the server indicating the error that cause this
         */
        void onSignupFailed(String error);
    }

    private OnRegistrationListener mOnRegistrationListener;
    private Task                   mTask;
    private String[]               mData;
    private String                 mUserId;
    private String                 mError;

    /**
     * 
     * @param list
     *            listener to which communicate the outcome of the operation
     * @param userData
     *            Data to transmit to the server to perform requested operation.
     *            <ul>
     *            <li>LOGIN: [email, password]</li>
     *            <li>LOGOUT: [email, password, name]</li>
     *            <li>SIGNUP: [userId]</li>
     *            </ul>
     */
    public RegistrationManager(OnRegistrationListener list, String[] userData) {
        mOnRegistrationListener = list;
        if (userData == null) {
            mData = new String[0];
        } else {
            mData = Arrays.copyOf(userData, userData.length);
        }
    }

    @Override
    protected Void doInBackground(Task... params) {
        if(!isCancelled()){
            mTask = params[0];
            switch (mTask) {
                case LOGIN:
                    mUserId = doLogin(false);
                    break;
                case LOGOUT:
                    mUserId = doLogout();
                    break;
                case SIGNUP:
                    mUserId = doLogin(true);
                    break;
                default:
                    // do nothing
                    Log.d(TAG, Consts.UNKNOWN_OPTION);
                    break;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if(!isCancelled()){
            switch (mTask) {
                case LOGIN:
                    if(mUserId != null) {
                        mOnRegistrationListener.onLoginSuccessful(mUserId);
                    } else {
                        mOnRegistrationListener.onLoginFailed(mError);
                    }
                    break;
                case LOGOUT:
                    if(mUserId != null) {
                        mOnRegistrationListener.onLogoutSuccessful(mUserId);
                    } else {
                        mOnRegistrationListener.onLogoutFailed(mError);
                    }
                    break;
                case SIGNUP:
                    if(mUserId != null) {
                        mOnRegistrationListener.onSignupSuccessful(mUserId);
                    } else {
                        mOnRegistrationListener.onSignupFailed(mError);
                    }
                    break;
                default:
                    // do nothing
                    Log.d(TAG, Consts.UNKNOWN_OPTION);
                    break;
            }
        }
    }

    /**
     * Performs the Login and Signup operations
     *  
     * @param signup whether we're doing Login or Signup
     * 
     * @return the userToken representing the user
     */
    private String doLogin(boolean signup) {
        String email = mData[0];
        String pwd = mData[1];
        String path = signup ? "signup" : "login";
        String result = null;
        try {
            String name = signup ? "&name=" + URLEncoder.encode(mData[2], Consts.UTF8) : "";
            String url = Consts.WEB_SERVER_URL_NOPORT + "/" + path + "/?mail=" + URLEncoder.encode(email, Consts.UTF8) + "&passw=" + URLEncoder.encode(pwd, Consts.UTF8) + name;
            Log.d(TAG, url);         
            result = GetRequest.getHTML(url);
            if (result != null) {
                JSONObject obj = new JSONObject(result);
                String status = obj.getString(Consts.STATUS);
                if (status.equals(Consts.SUCCESS)) {
                    MapMakerApplication.getPreferences().edit().putString(Consts.SESSION_ID, obj.getString(Consts.SESSION_ID)).commit();
                    if(obj.has(Consts.SUPER)) {
                        MapMakerApplication.getPreferences().edit().putBoolean(Consts.SUPER, obj.getBoolean(Consts.SUPER)).commit();
                    }
                    MapMakerApplication.getPreferences().edit().putString(Consts.USER_TOKEN, obj.getString(Consts.USER_TOKEN)).commit();
                    MapMakerApplication.getPreferences().edit().putString(Consts.OWNER_EMAIL, email).commit();
                    MapMakerApplication.getPreferences().edit().putString(Consts.OWNER, obj.getString(Consts.OWNER)).commit();
                    return obj.getString(Consts.USER_TOKEN);
                } else {
                    mError = obj.getString(Consts.ERROR);
                    return null;
                }
            }
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage());
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    /**
     * performs the Logout operation
     * 
     * @return the userToken representing the user who just logged out
     */
    private String doLogout() {
        String id = mData[0];
        String result = null;
        try {
            result = GetRequest.getHTML(Consts.WEB_SERVER_URL_NOPORT + "/logout/?userId=" + URLEncoder.encode(id, Consts.UTF8));
            if (result != null) {
                JSONObject obj = new JSONObject(result);
                String status = obj.getString(Consts.STATUS);
                if (status.equals(Consts.SUCCESS)) {
                    MapMakerApplication.getPreferences().edit().putString(Consts.SESSION_ID, null).commit();
                    return obj.getString(Consts.USER_TOKEN);
                } else {
                    mError = obj.getString(Consts.ERROR);
                    return null;
                }
            }
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage());
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

}
