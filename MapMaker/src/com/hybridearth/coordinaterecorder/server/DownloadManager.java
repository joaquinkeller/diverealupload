/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.server;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Environment;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.activities.BaseActivity;
import com.hybridearth.coordinaterecorder.objects.MapMakerMarker;
import com.hybridearth.coordinaterecorder.objects.MapOverlay;
import com.hybridearth.coordinaterecorder.objects.MetaioMarker;
import com.hybridearth.coordinaterecorder.objects.PanoramaMarker;
import com.hybridearth.coordinaterecorder.objects.PanoramaMarker.Type;
import com.hybridearth.coordinaterecorder.utils.Log;
import com.hybridearth.coordinaterecorder.utils.Utils;
import com.hybridearth.coordinaterecorder.utils.Utils.Task;

/**
 * takes care of downloading/uploading data from server.
 * Specifically: <ul>
 * <li>downloading lists of Overlays/Panoramas/Markers;</li>
 * <li>downloading images from a given URL;</li>
 * <li>uploading lists of Overlays/Panoramas/Markers to the server</li>
 * </ul> 
 * 
 * @author sciarcia
 * 
 */
@SuppressLint("DefaultLocale")
public class DownloadManager<T extends MapMakerMarker> extends AsyncTask<Task, Void, Void> {

    private static final String TAG = "DownloadManager";

    /**
     * Interface for callback methods on Network operations
     * 
     * @author sciarcia
     *
     */
    public interface NetworkTransferListener {
        /**
         * Method called when a list of Markers is downloaded
         * 
         * @param markers
         *            the list of markers retrieved from the server, or null if
         *            an error occured
         */
        void onMetaioMarkersDownloaded(List<MetaioMarker> markers);

        /**
         * Method called when a list of Panoramas is downloaded
         * 
         * @param panos
         *            the list of panoramas retrieved from the server, or null if
         *            an error occured
         */
        void onPanoramaMarkersDownloaded(List<PanoramaMarker> panos);

        /**
         * Method called when a list of map overlays is downloaded. The overlays
         * are usually building plans.
         * 
         * @param overlays
         *            the list of overlays retrieved from the server, or null if
         *            an error occured
         */
        void onOverlaysDownloaded(List<MapOverlay> overlays);

        /**
         * method called when an image has been downloaded to the device
         * 
         * @param imgPath
         *            the path of the device on the device
         */
        void onImageDowloaded(MapMakerMarker marker, String imgPath);
        
        /**
         * method called when a list of Overlays/Panoramas/Markers has been uploaded to the server
         */
        void onMarkersUploaded();
    }

    private NetworkTransferListener mCtx;
    private Task                    mTask;
    private List<MetaioMarker>      mMarkers;
    private List<PanoramaMarker>    mPanos;
    private List<MapOverlay>        mOverlays;
    private List<T>                 mUploading;
    private MapMakerMarker          mMmm;
    private String                  imgPath;

    public DownloadManager(NetworkTransferListener ctx) {
        this.mCtx = ctx;
    }

    public DownloadManager(NetworkTransferListener ctx, List<T> objs) {
        this(ctx);
        this.mUploading = objs;
    }

    public DownloadManager(NetworkTransferListener ctx, MapMakerMarker mmm) {
        this(ctx);
        this.mMmm = mmm;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!isCancelled()) {
            MapMakerApplication.getInstance().threadStart();
            if (mCtx instanceof BaseActivity) {
                ((BaseActivity) mCtx).setProgressBarIndeterminateVisibility(true);
            }
        } else {
            cancel(true);
        }
    }

    @Override
    protected Void doInBackground(Task... tasks) {
        mTask = tasks[0];
        switch (mTask) {
            case DOWNLOAD_MARKERS:
                mMarkers = downloadMetaioMarkers();
                break;
            case DOWNLOAD_PANOS:
                mPanos = downloadPanoramaMarkers();
                break;
            case DOWNLOAD_OVERLAYS:
                mOverlays = downloadOverlays();
                break;
            case UPLOAD_MARKERS:
                uploadData(Consts.IDMARKERS_SERVICE);
                break;
            case UPLOAD_PANOS:
                uploadData(Consts.PANORAMA_SERVICE);
                break;
            case UPLOAD_OVERLAYS:
                uploadData(Consts.MAPS_SERVICE);
                break;
            case DOWNLOAD_IMAGE:
                imgPath = downloadImg();
                break;
            default:
                // do nothing
                Log.e(TAG, Consts.UNKNOWN_TASK);
                break;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        MapMakerApplication.getInstance().threadStop();
        super.onPostExecute(result);
        if (!isCancelled()) {
            if (mCtx instanceof BaseActivity && !MapMakerApplication.getInstance().isDownloading()) {
                ((BaseActivity) mCtx).setProgressBarIndeterminateVisibility(false);
            }
            switch (mTask) {
                case DOWNLOAD_MARKERS:
                    mCtx.onMetaioMarkersDownloaded(mMarkers);
                    break;
                case DOWNLOAD_PANOS:
                    mCtx.onPanoramaMarkersDownloaded(mPanos);
                    break;
                case DOWNLOAD_OVERLAYS:
                    mCtx.onOverlaysDownloaded(mOverlays);
                    break;
                case DOWNLOAD_IMAGE:
                    mCtx.onImageDowloaded(mMmm, imgPath);
                    break;
                case UPLOAD_MARKERS:
                case UPLOAD_PANOS:
                case UPLOAD_OVERLAYS:
                    mCtx.onMarkersUploaded();
                    break;
                default:
                    // do nothing
                    Log.e(TAG, Consts.UNKNOWN_TASK);
                    break;
            }
        }
    }

    /**
     * Downloads an overlay/panorama/marker image from the server
     *   
     * @return image path on device storage
     */
    private String downloadImg() {
        String folder = null;
        String url = null;
        if (mMmm instanceof PanoramaMarker) {
            folder = Consts.PANORAMA_SERVICE;
            url = Consts.WEB_SERVER_URL_NOPORT + "/" + Consts.PANO_IMAGE + "/" + mMmm.getPicPath().substring(mMmm.getPicPath().lastIndexOf('/') + 1);
        } else if (mMmm instanceof MetaioMarker) {
            folder = Consts.MARKERPICS;
            url = Consts.WEB_SERVER_URL_NOPORT + "/" + folder + "/" + mMmm.getPicPath().substring(mMmm.getPicPath().lastIndexOf('/') + 1); 
        } else if (mMmm instanceof MapOverlay) {
            folder = Consts.MAPSPICS;
            url = Consts.WEB_SERVER_URL_NOPORT + "/" + folder + "/" + mMmm.getPicPath().substring(mMmm.getPicPath().lastIndexOf('/') + 1);
        }
        Log.d(TAG, "downloading img in " + folder);
        return Utils.downloadImage(mMmm.getPicPath(), folder, url);
    }

    /**
     * builds the URL to call to perform an operation
     * 
     * @param service the service we're calling (overlays, panoramas, markers)
     * @param post are we executing a POST or a GET? in case of GET we add the user location to the URL parameters
     * 
     * @return the URL to call
     */
    private String buildUrl(String service, boolean post) {
        StringBuilder url = new StringBuilder();
        url.append(Consts.WEB_SERVER_URL_NOPORT + "/" + service + "/");
        try {
             url.append("?" + Consts.USER_TOKEN + "=" + URLEncoder.encode(MapMakerApplication.getPreferences().getString(Consts.USER_TOKEN, ""), Consts.UTF8));
             url.append("&" + Consts.SESSION_ID + "=" + URLEncoder.encode(MapMakerApplication.getPreferences().getString(Consts.SESSION_ID, ""), Consts.UTF8));
             if(!post){
                 Location l = MapMakerApplication.getLocationManager().getCurrentLocation();
                 if(l != null) {
                     url.append("&" + Consts.LAT + "=" + l.getLatitude());
                     url.append("&" + Consts.LON + "=" + l.getLongitude());
                 }
                 String limit = MapMakerApplication.getPreferences().getString(Consts.LIMIT, String.valueOf(Consts.LIMIT_DEFAULT));
                 url.append("&" + Consts.LIMIT + "=" + limit);
             }
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage());
        }
        return url.toString();
    }
    
    /**
     * builds the URL to call to perform an operation
     * 
     * @param service the service we're calling (overlays, panoramas, markers)
     * 
     * @return the URL to call
     */
    private String buildUrl(String service) {
        return buildUrl(service, false);
    }
    
    /**
     * perform the action of calling the server to retrieve a list of markers
     * 
     * @return a List of {@link MetaioMarker} objects
     */
    private List<MetaioMarker> downloadMetaioMarkers() {
        String url = buildUrl(Consts.IDMARKERS_SERVICE);
        List<MetaioMarker> ms = null;
        String returned = GetRequest.getHTML(url);
        if (returned == null) {
            Log.d(TAG, Consts.NULL_RESPONSE);
        } else {
            try {
                JSONObject obj = new JSONObject(returned);
                JSONArray array = (JSONArray) obj.get("id"+Consts.IDMARKERS_SERVICE);
                ms = new ArrayList<MetaioMarker>(array.length());
                for (int i = 0; i < array.length(); i++) {
                    JSONObject item = (JSONObject) array.get(i);
                    String id = item.getString(Consts.ID);
                    JSONObject position = (JSONObject) item.get(Consts.POSITION);
                    double lat = Double.valueOf(position.getString(Consts.LAT));
                    double lon = Double.valueOf(position.getString(Consts.LON));
                    LatLng pos = new LatLng(lat, lon);
                    int angle = item.getInt(Consts.ANGLE);
                    String path = null;
                    if (item.has(Consts.PATH)) {
                        // here it's still just the file name from the server,
                        // with no path!
                        path = item.getString(Consts.PATH);
                    }
                    int altitude = item.getInt(Consts.ALTITUDE);
                    int width = item.getInt(Consts.WIDTH);
                    int metaioid = item.getInt(Consts.METAIO_ID);
                    if(path != null){
                        path = Environment.getExternalStorageDirectory() + "/" + Consts.MAP_MAKER + "/" + Consts.MARKERPICS + "/" + path.substring(path.lastIndexOf('/') + 1);
                    }
                    MetaioMarker mm = new MetaioMarker(id, pos, angle, path, altitude, width, metaioid);
                    if(item.has(Consts.EDITABLE)){
                        mm.setEditable(item.getBoolean(Consts.EDITABLE));
                    }
                    if(item.has(Consts.OWNER)){
                        mm.setOwner(item.getString(Consts.OWNER));
                    }
                    if(item.has(Consts.OWNER_EMAIL)){
                        mm.setOwnerEmail(item.getString(Consts.OWNER_EMAIL));
                    }
                    if(item.has(Consts.USER_TOKEN)) {
                        mm.setUserToken(item.getString(Consts.USER_TOKEN));
                    }
                    ms.add(mm);
                }
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return ms;
    }

    /**
     * perform the action of calling the server to retrieve a list of panoramas
     * 
     * @return a List of {@link PanoramaMarker} objects
     */
    private List<PanoramaMarker> downloadPanoramaMarkers() {
        String url = buildUrl(Consts.PANORAMA_SERVICE);
        List<PanoramaMarker> ms = null;
        String returned = GetRequest.getHTML(url);
        if (returned == null) {
            Log.d(TAG, Consts.NULL_RESPONSE);
        } else {
            try {
                JSONObject obj = new JSONObject(returned);
                JSONArray array = (JSONArray) obj.get(Consts.PANORAMA_SERVICE);
                ms = new ArrayList<PanoramaMarker>(array.length());
                for (int i = 0; i < array.length(); i++) {
                    JSONObject item = (JSONObject) array.get(i);
                    String id = item.getString(Consts.ID);
                    JSONObject position = (JSONObject) item.get(Consts.POSITION);
                    double lat = Double.valueOf(position.getString(Consts.LAT));
                    double lon = Double.valueOf(position.getString(Consts.LON));
                    LatLng pos = new LatLng(lat, lon);
                    int angle = item.getInt(Consts.ANGLE);
                    String path = null;
                    if (item.has(Consts.PATH)) {
                        // here it's still just the file name from the server,
                        // with no path!
                        path = item.getString(Consts.PATH);
                        if(path != null && !"".equals(path)){
                            path = Environment.getExternalStorageDirectory() + "/" + Consts.MAP_MAKER + "/" + Consts.PANOS + "/" + path.substring(path.lastIndexOf('/') + 1);
                        }
                    }
                    JSONArray neighborsJSON = (JSONArray) item.get(Consts.NEIGHBORS);
                    List<PanoramaMarker> neighbors = new ArrayList<PanoramaMarker>();
                    if (neighborsJSON != null) {
                        for (int j = 0; j < neighborsJSON.length(); j++) {
                            JSONObject neighbor = (JSONObject) neighborsJSON.get(j);
                            String neighborID = neighbor.getString(Consts.ID);
                            double nLat = Double.valueOf(((JSONObject) neighbor.get(Consts.POSITION)).getString(Consts.LATITUDE));
                            double nLon = Double.valueOf(((JSONObject) neighbor.get(Consts.POSITION)).getString(Consts.LONGITUDE));
                            neighbors.add(new PanoramaMarker(neighborID, new LatLng(nLat, nLon)));
                        }
                    }
                    Type type = Type.INNER.toString().equalsIgnoreCase(item.getString(Consts.STATUS)) ? Type.INNER : Type.ENTRANCE;
                    double panoCenter = item.getInt(Consts.ANGLE_OFFSET);
                    double pitch = item.getInt(Consts.PITCH);
                    int cameraAlt = item.getInt(Consts.CAMERA_ALT);
                    PanoramaMarker pm = new PanoramaMarker(id, pos, angle, path, type, panoCenter, neighbors, cameraAlt, pitch);
                    if(item.has(Consts.EDITABLE)){
                        pm.setEditable(item.getBoolean(Consts.EDITABLE));
                    }
                    if(item.has(Consts.OWNER)){
                        pm.setOwner(item.getString(Consts.OWNER));
                    }
                    if(item.has(Consts.OWNER_EMAIL)){
                        pm.setOwnerEmail(item.getString(Consts.OWNER_EMAIL));
                    }
                    if(item.has(Consts.USER_TOKEN)) {
                        pm.setUserToken(item.getString(Consts.USER_TOKEN));
                    }
                    ms.add(pm);
                }
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return ms;
    }

    /**
     * perform the action of calling the server to retrieve a list of overlays
     * 
     * @return a List of {@link MapOverlay} objects
     */
    private List<MapOverlay> downloadOverlays() {
        String url = buildUrl(Consts.MAPS_SERVICE);
        List<MapOverlay> os = null;
        String returned = GetRequest.getHTML(url);
        if (returned == null) {
            Log.d(TAG, Consts.NULL_RESPONSE);
        } else {
            try {
                JSONObject obj = new JSONObject(returned);
                JSONArray array = (JSONArray) obj.get(Consts.MAPS_SERVICE);
                os = new ArrayList<MapOverlay>(array.length());
                for (int i = 0; i < array.length(); i++) {
                    JSONObject item = (JSONObject) array.get(i);
                    String id = item.getString(Consts.ID);
                    JSONArray pos = (JSONArray) item.get(Consts.ANCHOR);
                    double lat = Double.valueOf(pos.getString(0));
                    double lon = Double.valueOf(pos.getString(1));
                    LatLng anchor = new LatLng(lat, lon);
                    float width = Float.valueOf(item.getString(Consts.WIDTH));
                    float height = Float.valueOf(item.getString(Consts.HEIGHT));
                    float bearing = Float.valueOf(item.getString(Consts.BEARING));
                    String path = null;
                    if(item.has(Consts.PATH)){
                        path = item.getString(Consts.PATH);
                    } else {
                        /*
                         * WARNING!!!!
                         * This takes care of some weird edge cases coming out from nowhere,
                         * where an overlay has no "path" attribute.
                         * 
                         * Why is this dangerous? Because it hardcodes the remote path on server.
                         * If that ever changes, this will not work anymore
                         */
                        path = "maps/maps-" + id + ".jpg";
                    }
                    if (path != null) {
                        path = Environment.getExternalStorageDirectory() + "/" + Consts.MAP_MAKER + "/" + Consts.MAPSPICS + "/" + path.substring(path.lastIndexOf('/') + 1);
                        path = Utils.downloadImage(path, Consts.MAPSPICS);
                    }
                    MapOverlay mo = new MapOverlay(id, anchor, width, height, path, bearing);
                    if(item.has(Consts.EDITABLE)){
                        mo.setEditable(item.getBoolean(Consts.EDITABLE));
                    }
                    if(item.has(Consts.OWNER)){
                        mo.setOwner(item.getString(Consts.OWNER));
                    }
                    if(item.has(Consts.OWNER_EMAIL)){
                        mo.setOwnerEmail(item.getString(Consts.OWNER_EMAIL));
                    }
                    if(item.has(Consts.USER_TOKEN)) {
                        mo.setUserToken(item.getString(Consts.USER_TOKEN));
                    }
                    os.add(mo);
                }
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return os;
    }

    /**
     * Sends data to the server. Data to send is set in the constructor by the caller
     * 
     * @param service the service to call
     */
    private void uploadData(String service) {
        for (MapMakerMarker m : mUploading) {
            int returnCode = -1;
            switch (mTask) {
                case UPLOAD_MARKERS:
                case UPLOAD_PANOS:
                case UPLOAD_OVERLAYS:
                    /*
                     * WARNING!!!!
                     * this isn't nice but it is necessary to avoid some nasty
                     * stackoverflowerror in Gson.toJson due to circular dependency between neighbors 
                     */
                    List<PanoramaMarker> neighbors = null;
                    if(m instanceof PanoramaMarker) {
                        neighbors = ((PanoramaMarker)m).getNeighbors();
                        List<PanoramaMarker> tmp = new ArrayList<PanoramaMarker>(neighbors.size());
                        for(PanoramaMarker n : neighbors) {
                            tmp.add(new PanoramaMarker(n.getId(), n.getPosition()));
                        }
                        ((PanoramaMarker) m).setNeighbors(tmp);
                    }
                    returnCode = FileUpload.doUpload(new Gson().toJson(m), m.getId(), m.getPicPath(), buildUrl(service, true), m.isChangedPicture());
                    if(m instanceof PanoramaMarker && neighbors != null) {
                        ((PanoramaMarker) m).setNeighbors(neighbors);
                    }
                    break;
                default:
                    // do nothing
                    Log.e(TAG, Consts.UNKNOWN_TASK);
                    break;
            }
            Log.d(TAG, returnCode + "");
        }
    }

}
