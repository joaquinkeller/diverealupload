/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.hybridearth.coordinaterecorder.utils.Log;

import android.net.http.AndroidHttpClient;

/**
 * Takes care of performing the actual POST of data to the server
 * 
 * @author triponez
 * @author sciarcia
 * 
 */
public class FileUpload {

    private static final String TAG = "FileUpload";

    private static final String FILE_NAME = "filename";
    private static final String FILE_DATA = "file_data";
    private static final String UPLOADED_FILE = "uploaded_file";
    
    private FileUpload(){
        //not to be instantiated
    }
    
    /**
     * Uploads an object to the server
     * 
     * @param jsonObject a string representing the object (json-encoded)
     * @param id the id of the object we're sending
     * @param path if the object has an image associated this is the path on disk
     * @param urlServer the URL of the server to call
     * @param sendPic whether to send the picture to the server or not
     * 
     * @return status code of the response
     */
    public static int doUpload(String jsonObject, String id, String path, String urlServer, boolean sendPic) {
        if(path == null || "".equals(path) || !sendPic){
            return doPost(jsonObject, urlServer);
        } else {
            return uploadFile(jsonObject, id, path, urlServer + "&img=1");
        }
    }

    /**
     * Post to server without images
     * 
     * @param jsonObject a string representing the object (json-encoded)
     * @param urlString the URL of the server to call
     * 
     * @return status code of the response or 0 if some problem occured
     */
    public static int doPost(String jsonObject, String urlString) {
        HttpResponse response = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            String urlServer = urlString + "&img=0";
            HttpPost httppost = new HttpPost(urlServer);

            // Request parameters and other properties.
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(FILE_DATA, jsonObject));
            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

            // Execute and get the response.
            response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(instream));

                    String result;
                    result = "";
                    String line = bufferedReader.readLine();
                    while (line != null) {
                        result += line;
                        line = bufferedReader.readLine();
                    }
                    bufferedReader.close();

                    Log.d(TAG, result);
                } finally {
                    instream.close();
                }
            }
        } catch (UnsupportedEncodingException e) {
            Log.d(TAG, e.getMessage());
        } catch (IOException ioe) {
            Log.d(TAG, ioe.getMessage());
        }
        if(response != null) {
            return response.getStatusLine().getStatusCode();
        } else {
            return 0;
        }
    }

    /**
     * Post to server with image, delegates the actual operation to {@link FileUpload#postData(MultipartEntity, String)}
     * 
     * @param jsonObject a string representing the object (json-encoded)
     * @param id the id of the object we're sending
     * @param path if the object has an image associated this is the path on disk
     * @param urlServer the URL of the server to call
     * 
     * @return status code of the response
     */
    private static int uploadFile(String jsonObject, String id, String path, String urlServer) {
        int rcode = -1;
        MultipartEntity dataToSend = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        try {
            dataToSend.addPart(FILE_NAME, new StringBody(String.valueOf(id)));
            dataToSend.addPart(FILE_DATA, new StringBody(jsonObject));
            dataToSend.addPart(UPLOADED_FILE, new FileBody(new File(path)));
            rcode = postData(dataToSend,urlServer);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        return rcode;
    }

    /**
     * Performs the actual send action
     * 
     * @param data the binary data to transmit to server
     * @param url the URL of the server to call
     * 
     * @return status code of the response
     */
    private static int postData(MultipartEntity data, String url) {
        int resp = -1;
        HttpResponse hresp = null;
        try {
            Log.d(TAG, "Post URL is " + url);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(data);
            HttpClient httpClient = AndroidHttpClient.newInstance("HybridEarth");
            hresp = httpClient.execute(httpPost);
            String responseString = EntityUtils.toString(hresp.getEntity());
            Log.d(TAG, responseString);
            resp = hresp.getStatusLine().getStatusCode();
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage());
        } catch (ClientProtocolException e) {
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return resp;
    }
    
}
