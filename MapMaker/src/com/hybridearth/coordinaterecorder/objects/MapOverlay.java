/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.objects;

import com.google.android.gms.maps.model.LatLng;
import com.hybridearth.coordinaterecorder.utils.Utils;

/**
 * Represents a GroundOverlay on a GoogleMap.
 * It's used to put the "fire escape map" on top of a building, to better locate where to put markers/panoramas
 * 
 * @author triponez
 * @author sciarcia
 * 
 */
public class MapOverlay extends MapMakerMarker {

    private static final long serialVersionUID = 1L;
    private float             width;
    private float             height;
    private float             bearing;

    public MapOverlay(LatLng position, float width, float height, String imageUri) {
        this(position, width, height, imageUri, 0);
    }

    public MapOverlay(LatLng position, float width, float height, String imageUri, float mapBearing) {
        this(Utils.nextSessionId(), position, width, height, imageUri, mapBearing);
    }

    public MapOverlay(String id, LatLng position, float width, float height, String imageUri, float mapBearing) {
        super(id, position, 0, imageUri);
        this.width = width;
        this.height = height;
        this.bearing = mapBearing;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }
}
