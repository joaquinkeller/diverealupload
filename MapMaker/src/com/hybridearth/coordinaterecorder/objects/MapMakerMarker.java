/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.objects;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;
import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.MapMakerApplication.AppState;

/**
 * base class for objects in the map with common fields
 * 
 * @author triponez
 * @author sciarcia
 * 
 */
public class MapMakerMarker implements Serializable {

    private static final long serialVersionUID = 1L;
    private String            id;
    private LatLng            position;
    private double            angle;
    private String            picPath;
    private boolean           delete;
    private boolean           editable;
    private String            owner;
    private String            ownerEmail;
    private String            userToken;
    private boolean           changedPicture;

    public MapMakerMarker(String id, LatLng position) {
        this.id = id;
        this.position = position;
        this.delete = false;
        // default visibility = private
        this.editable = false;
        this.changedPicture = false;
    }

    public MapMakerMarker(String id, LatLng position, double angle, String picPath) {
        this(id, position);
        this.angle = angle;
        this.picPath = picPath;
    }

    /**
     * this will set userToken, owner and ownerEmail with values from SharedPreferences
     */
    public void setUserData() {
        this.userToken = MapMakerApplication.getPreferences().getString(Consts.USER_TOKEN, "");
        this.owner = MapMakerApplication.getPreferences().getString(Consts.OWNER, "");
        this.ownerEmail = MapMakerApplication.getPreferences().getString(Consts.OWNER_EMAIL, "");
    }

    /**
     * check if i can edit this marker
     * 
     * @return true if I'm in edit mode and <br/>
     *         either it's publicly editable or<br/>
     *         it's not editable but i'm super user or<br/>
     *         it's mine
     */
    public boolean canIEdit() {
        return MapMakerApplication.getAppState() == AppState.EDIT
                && (this.isEditable() || this.getUserToken().equals(MapMakerApplication.getPreferences().getString(Consts.USER_TOKEN, "")) || (!this.isEditable() && MapMakerApplication
                        .getPreferences().getBoolean(Consts.SUPER, false)));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public boolean isChangedPicture() {
        return changedPicture;
    }

    public void setChangedPicture(boolean changedPicture) {
        this.changedPicture = changedPicture;
    }

}
