/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.objects;

import com.google.android.gms.maps.model.LatLng;

/**
 * represent a marker (from metaio, kind of a big qr code)
 * 
 * @author sciarcia
 * 
 */
public class MetaioMarker extends MapMakerMarker {

    private static final long serialVersionUID = 1L;
    private int               altitude;
    private int               width;
    private int               metaioID;

    public MetaioMarker(String id, LatLng position) {
        super(id, position);
    }
    
    public MetaioMarker(String id, LatLng position, double angle, String path, int altitude, int width, int metaioID) {
        super(id, position, angle, path);
        this.altitude = altitude;
        this.width = width;
        this.metaioID = metaioID;
    }
    
    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getMetaioID() {
        return metaioID;
    }

    public void setMetaioID(int metaioID) {
        this.metaioID = metaioID;
    }
}
