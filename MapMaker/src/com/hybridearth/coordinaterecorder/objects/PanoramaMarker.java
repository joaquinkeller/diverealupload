/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.objects;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;

/**
 * represents a panorama, i.e. a spherical picture of a place (usually taken with SphotoSphere)
 * @author sciarcia
 * 
 */
public class PanoramaMarker extends MapMakerMarker {

    private static final long serialVersionUID = 1L;

    /**
     * defines what kind of panorama this is.
     * <ul>
     * <li>INNER: a panorama inside a building</li>
     * <li>ENTRANCE: this can also be inside a building, literally, but it just means that it's kind of a bridge between INNER panoramas and StreetView.</li>
     * </ul>
     */
    public enum Type {
        INNER("Inner"), ENTRANCE("Entrance");

        private final String text;

        private Type(final String text) {
            this.text = Character.toUpperCase(text.charAt(0)) + text.substring(1);
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private Type                 type;
    private double               pitch;
    private double               panoCenter;
    private List<PanoramaMarker> neighbors;
    private double               cameraAlt;

    public PanoramaMarker(String id, LatLng position) {
        super(id, position);
        // default type = Inner
        this.type = Type.INNER;
        this.neighbors = new ArrayList<PanoramaMarker>();
    }

    public PanoramaMarker(String id, LatLng position, double angle, String path, Type type, double panoCenter, List<PanoramaMarker> neighbors, double cameraAlt, double pitch) {
        super(id, position, angle, path);
        this.type = type;
        this.panoCenter = panoCenter;
        this.neighbors = neighbors;
        this.cameraAlt = cameraAlt;
        this.pitch = pitch;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public double getPitch() {
        return pitch;
    }

    public void setPitch(double pitch) {
        this.pitch = pitch;
    }

    public double getPanoCenter() {
        return panoCenter;
    }

    public void setPanoCenter(double panoCenter) {
        this.panoCenter = panoCenter;
    }

    public List<PanoramaMarker> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(List<PanoramaMarker> neighbors) {
        this.neighbors = neighbors;
    }

    /**
     * checks if the given panorama is a neighbor of this one
     * 
     * @param check the panorama we want to know if it's our neighbor
     * @return true if the given panorama is our neighbor
     */
    public boolean hasNeighbor(PanoramaMarker check) {
        for (PanoramaMarker n : this.neighbors) {
            if (n.getId().equals(check.getId())) {
                return true;
            }
        }
        return false;
    }

    public double getCameraAlt() {
        return cameraAlt;
    }

    public void setCameraAlt(double cameraAlt) {
        this.cameraAlt = cameraAlt;
    }

    /**
     * removes the specific panorama from our list of neighbors
     * 
     * @param remove the panorama we want to remove from the list of neighbors
     */
    public void removeNeighbor(PanoramaMarker remove) {
        String removeId = remove.getId();
        for (PanoramaMarker n : neighbors) {
            if (n.getId().equals(removeId)) {
                neighbors.remove(n);
                break;
            }
        }
    }

    /**
     * utility method to print a human-readable version of the list of our neighbors
     * 
     * @return a string representing our list of neighbors
     */
    public String printNeighborsList() {
        StringBuilder sb = new StringBuilder();
        sb.append("Panorama " + getId() + " - Neighbors: ");
        for (PanoramaMarker pm : neighbors) {
            sb.append(pm.getId() + " ");
        }
        return sb.toString();
    }
    
}
