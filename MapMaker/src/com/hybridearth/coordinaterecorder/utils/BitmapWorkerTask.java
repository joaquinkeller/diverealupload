/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.utils;

import java.lang.ref.WeakReference;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.TypedValue;
import android.widget.ImageView;

import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.R;

/**
 * Takes care of loading an image into an imageView.
 * This task is computationally heavy, so we offload it here, on a different thread than the UI one
 * 
 * @author sciarcia
 * 
 */
public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewReference;
    private int reqWidth = 0;
    private int reqHeight = 0;

    public BitmapWorkerTask(ImageView imageView) {
        imageViewReference = new WeakReference<ImageView>(imageView);
        reqWidth = imageView.getWidth();
        reqHeight = imageView.getHeight();
        /*
         * the following happens on a popup dialog.
         * We have the spinning thing and the ImageView is GONE, so its dimensions are 0.
         */
        if(reqWidth == 0 || reqHeight == 0) {
            Resources r = MapMakerApplication.getAppContext().getResources();
            reqWidth = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, r.getDimension(R.dimen.preview_width), r.getDisplayMetrics()));
            reqHeight = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, r.getDimension(R.dimen.preview_width), r.getDisplayMetrics()));
        }    }

    /**
     * @param params [0]  is the path on disk of the image to load 
     */
    @Override
    protected Bitmap doInBackground(String... params) {
        String panoPath = params[0];
        if(panoPath != null) {
            return Utils.getSampledBitmapFromPathResource(panoPath, reqWidth, reqHeight);
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }
}
