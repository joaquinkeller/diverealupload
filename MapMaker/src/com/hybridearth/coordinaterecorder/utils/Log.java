/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.utils;

import com.hybridearth.coordinaterecorder.Consts;

/**
 * Wrapper around {@link android.util.Log}.
 * Nothing new here, but there's a flag we use to turn loggin on/off.
 * 
 * If you wonder why it's called <i>Log</i> as its android counterpart, 
 * it's because this has been created after the code was already pretty big and I didn't feel like changing a whole bunch of code around 20 classes.
 * Having the same name allowed me to Ctrl+Shift+O and fix imports only.
 * 
 * @author sciarcia
 * 
 */
public class Log {

    private static final String TAG = "MapMakerLog";

    /**
     * Mimicks normal android logging levels
     * 
     * @author sciarcia
     *
     */
    public enum Level {
        ERROR, WARNING, DEBUG, VERBOSE, INFO, WTF
    }
    
    /**
     * make this false and you'll see no more Logs
     */
    private static boolean debug = false;

    /**
     * performs the loggin using the android logger
     * 
     * @param l level of logging, see {@link Log#Level}
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    private static void log(Level l, String tag, String msg) {
        if(debug){
            if (msg == null) {
                msg = Consts.NULL_MESSAGE;
            }
            if(tag ==null){
                tag = TAG;
            }
            switch(l){
                case ERROR:
                    android.util.Log.e(tag, msg);
                    break;
                case DEBUG:
                    android.util.Log.d(tag, msg);
                    break;
                case WARNING:
                    android.util.Log.w(tag, msg);
                    break;
                case VERBOSE:
                    android.util.Log.v(tag, msg);
                    break;
                case INFO:
                    android.util.Log.i(tag, msg);
                    break;
                case WTF:
                default:
                    android.util.Log.wtf(tag, msg);
                    break;
            }
        }
    }

    /**
     * Sends a ERROR log message
     * 
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void e(String tag, String msg){
        log(Level.ERROR, tag, msg);
    }

    /**
     * Sends a DEBUG log message
     * 
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void d(String tag, String msg){
        log(Level.DEBUG, tag, msg);
    }

    /**
     * Sends a WARNING log message
     * 
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void w(String tag, String msg){
        log(Level.WARNING, tag, msg);
    }
    
    /**
     * Sends a VERBOSE log message
     * 
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void v(String tag, String msg){
        log(Level.VERBOSE, tag, msg);
    }

    /**
     * Sends a INFO log message
     * 
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void i(String tag, String msg){
        log(Level.INFO, tag, msg);
    }

    /**
     * What a Terrible Failure: Report a condition that should never happen. 
     * The error will always be logged at level ASSERT with the call stack. 
     * Depending on system configuration, a report may be added to the DropBoxManager and/or the process may be terminated immediately with an error dialog.
     * 
     * @param tag Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static void wtf(String tag, String msg){
        log(Level.WTF, tag, msg);
    }

}
