/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.LatLng;
import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.MapMakerApplication.AppState;
import com.hybridearth.coordinaterecorder.R;
import com.hybridearth.coordinaterecorder.server.RegistrationManager;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * Utility class
 * 
 * @author triponez
 * @author sciarcia
 * 
 */
public class Utils {

    private static final String TAG           = "Utils";

    /**
     * Indicates a task to accomplish.
     * This is used both in {@link DownloadManager} and {@link RegistrationManager}
     * @author sciarcia
     *
     */
    public enum Task {
        DOWNLOAD_MARKERS, DOWNLOAD_PANOS, DOWNLOAD_OVERLAYS, DOWNLOAD_IMAGE, UPLOAD_MARKERS, UPLOAD_PANOS, UPLOAD_OVERLAYS, LOGIN, LOGOUT, SIGNUP
    }

    private Utils() {
        // Exists only to defeat instantiation.
    }

    /**
     * checks if the SD card is available and it is possible to write on it
     * 
     * @return true if we can write on the SD card of the device
     */
    public static boolean isStorageWritable() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return true;
        }
        return false;
    }

    /**
     * Creates an id for new markers
     * 
     * @return an alphanumeric id
     */
    public static String nextSessionId() {
        return new BigInteger(24, new SecureRandom()).toString(32);
    }

    /**
     * Fetch the bitmap from disk
     * 
     * @param uri
     *            the Uri of the file in memory
     * @param reqWidth
     *            the required width
     * @param reqHeight
     *            the required height
     * @param imageWidth
     *            the original image width
     * @param imageHeight
     *            the orifinal image height
     * @return
     */
    public static Bitmap decodeSampledBitmapFromResource(Uri uri, int reqWidth, int reqHeight, int imageWidth, int imageHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        BitmapRegionDecoder decoder = null;
        Bitmap returned = null;
        try {
            Log.d(TAG, "decoding uri: " + uri);
            InputStream is = MapMakerApplication.getAppContext().getContentResolver().openInputStream(uri);
            decoder = BitmapRegionDecoder.newInstance(is, false);
            returned = decoder.decodeRegion(new Rect(0, 0, imageHeight, imageWidth), options);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return returned;
    }

    /**
     * Computes the sample size for an image
     * 
     * @param options
     *            the options containing the original size of the image on disk
     * @param reqWidth
     *            the required width for the image
     * @param reqHeight
     *            the required height for the image
     * @return the inSampleSize for the image to fetch
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        //double again 'just in case'
        return inSampleSize;
    }

    /**
     * Get the left position of this view in the root view
     * 
     * @param myView
     *            the {@link View} for which we want the relative left position
     * @return the relative left position of this view in its root view
     */
    public static int getRelativeLeft(View myView) {
        if (myView.getParent() == myView.getRootView()) {
            return myView.getLeft();
        } else {
            return myView.getLeft() + getRelativeLeft((View) myView.getParent());
        }
    }

    /**
     * Get the top position of this view in the root view
     * 
     * @param myView
     *            the {@link View} for which we want the relative top position
     * @return the relative top position of this view in its root view
     */
    public static int getRelativeTop(View myView) {
        if (myView.getParent() == myView.getRootView()) {
            return myView.getTop();
        } else {
            return myView.getTop() + +getRelativeTop((View) myView.getParent());
        }
    }

    /**
     * Computes the distance between two GPS coordinates
     * 
     * @param one
     *            the first point
     * @param two
     *            the second point
     * @return the distance in meters
     */
    public static float distanceBetween(LatLng one, LatLng two) {
        float[] results = new float[1];
        Location.distanceBetween(one.latitude, one.longitude, two.latitude, two.longitude, results);
        return results[0];
    }

    /**
     * Check if GoogleMaps application is installed on the phone
     * This is not so good, cause to perform this check it uses the hardcoded package of that app.
     * If this ever changes, this should change too
     * 
     * @return True if it is installed, False otherwise
     */
    public static boolean isGoogleMapsInstalled() {
        try {
            MapMakerApplication.getAppContext().getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
            return false;
        }
    }

    /**
     * Check if GooglePlay services are installed on the phone
     * 
     * @return True if it is installed, False otherwise
     */
    public static boolean checkGooglePlayServices(Activity act) {
        int checkGooglePlayServices = GooglePlayServicesUtil.isGooglePlayServicesAvailable(act);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            // google play services is missing
            Dialog errDialog = GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices, act, 1237);
            if (errDialog != null) {
                errDialog.show();
            }
            return false;
        }
        return true;
    }

    /**
     * Wrapper around {@link Utils#isGoogleMapsInstalled()}
     * Shos a toast if google Maps is not installed
     * 
     * @param act the activity calling this. Needed to show the toast
     * 
     * @return true if google maps is installed
     */
    public static boolean checkGoogleMaps(Activity act) {
        if (!isGoogleMapsInstalled()) {
            Builder builder = new AlertDialog.Builder(act);
            builder.setMessage(act.getString(R.string.installGoogleMaps)).setCancelable(false).setPositiveButton(act.getString(R.string.install), Utils.getGoogleMapsListener(act))
                    .create().show();
            return false;
        }
        return true;
    }

    /**
     * Get the listener for a click on the "install GoogleMaps" {@link Button}
     * of the {@link Dialog} that will be automatically displayed if the user's
     * phone doesn't have GoogleMaps application installed
     * 
     * @return the {@link android.view.View.OnClickListener} for the
     *         {@link Button}
     */
    public static OnClickListener getGoogleMapsListener(final Activity act) {
        return new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.maps"));
                act.startActivity(intent);
                // Finish the activity so they can't circumvent the check
                act.finish();
            }
        };
    }

    /**
     * Downloads an image from the server
     * 
     * @param path image path on disk to store the image into
     * @param folder the service we're calling on the server (Markers, Panoramas, Overlays)
     *  
     * @return the path on disk of the image
     */
    public static String downloadImage(String path, String folder) {
        if (path == null || "".equals(path) || folder == null || "".equals(folder)) {
            return null;
        }
        return downloadImage(path, folder, Consts.WEB_SERVER_URL_NOPORT + "/" + folder + "/" + path.substring(path.lastIndexOf('/') + 1));
    }

    /**
     * Downloads an image from the server
     * 
     * @param path image path on disk to store the image into
     * @param folder the service we're calling on the server (Markers, Panoramas, Overlays)
     * @param urlString URL of the server to call
     * 
     * @return the path on disk of the image
     */
    public static String downloadImage(String path, String folder, String urlString) {
        if (path == null || "".equals(path) || folder == null || "".equals(folder)) {
            return null;
        }
        try {
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();
            connection.connect();
            if (Utils.isStorageWritable()) {
                File mapsDir = new File(Environment.getExternalStorageDirectory(), "/" + Consts.MAP_MAKER + "/" + folder);
                boolean dir = false;
                if (!mapsDir.exists() || !mapsDir.isDirectory()) {
                    dir = mapsDir.mkdirs();
                } else {
                    dir = true;
                }
                if (dir) {
                    InputStream input = new BufferedInputStream(url.openStream());
                    OutputStream output = new FileOutputStream(path);

                    byte[] data = new byte[1024];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }

                    output.flush();
                    output.close();
                    input.close();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.e(TAG + " - downloadImage", e.getMessage());
            return null;
        }
        return path;
    }
    
    /**
     * Generates a new unique {@link Uri} to store a picture
     * 
     * @param ctx the calling activity
     * @return the URI associated with the saved picture 
     */
    public static Uri createUri(Context ctx) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, ctx.getFilesDir().getAbsolutePath() + "/IMG" + System.currentTimeMillis() + ".jpg");
        return MapMakerApplication.getAppContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    /**
     * Allows the user to crop the selected picture
     * 
     * @param act the activity calling this
     * @param imgUri the URI pointing to the image to crop
     */
    public static void performCrop(Activity act, Uri imgUri) {
        Intent intent = new Intent(act, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, getRealPathFromURI(imgUri));
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 0);
        intent.putExtra(CropImage.ASPECT_Y, 0);
        act.startActivityForResult(intent, Consts.CROP_PIC_REQUEST);
    }

    /**
     * Returns the {@link String} path to a resource, given its {@link Uri} on
     * the phone's memory
     * 
     * @param contentUri
     *            the {@link Uri} for which the {@link String} path is wanted
     * @return the path to the resource
     */
    public static String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = MapMakerApplication.getAppContext().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(columnIndex);
        } else {
            return null;
        }
    }

    /**
     * Starts the camera {@link Activity} for the user to take a new picture
     * 
     * @param act the calling activity
     * @param imgUri the URI where to store the image
     */
    public static void startCamera(Activity act, Uri imgUri) {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        Utils.createUri(act);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        act.startActivityForResult(cameraIntent, Consts.CAMERA_PIC_REQUEST);
    }

    /**
     * Starts the Gallery {@link Activity} for the user to pick a picture he has
     * already taken
     * 
     * @param act the calling activity
     */
    public static void startGallery(Activity act) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        act.startActivityForResult(pickPhoto, Consts.GALLERY_PIC_REQUEST);
    }

    /**
     * init a progress dialog to show while downloading stuff
     * 
     * @param ctx the calling activity
     * @return an initialized progressdialog to show messages to the user
     */
    public static ProgressDialog initDialog(Context ctx) {
        ProgressDialog pd = new ProgressDialog(ctx);
        pd.setMessage(ctx.getString(R.string.download_file));
        pd.setIndeterminate(false);
        pd.setMax(100);
        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        return pd;
    }

    /**
     * deletes user preferences and logs out immediately (doesn't call the server)
     */
    public static void wipeUserData() {
        MapMakerApplication.getPreferences().edit().clear().commit();
        MapMakerApplication.setAppState(AppState.LOGGED_OUT);
    }

    /**
     * Generate a random name
     * 
     * @return a random generated name
     */
    public static String randomNameAnonymous() {
        // Generate a random greek name;
        int min = 0;
        int max = greekLetters.length - 1;

        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomIndex = rand.nextInt(max - min + 1) + min;

        String name = greekLetters[randomIndex];

        // Generate a random number between 0 and 999;
        min = 0;
        max = 999;

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNumber = rand.nextInt(max - min + 1) + min;

        return name + randomNumber;
    }

    private static String[] greekLetters = { "Alpha", "Beta", "Gamma", "Delta", "Epsilon", "Zeta", "Eta", "Theta", "Iota", "Kappa", "Lambda", "Mu", "Nu", "Xi", "Omicron", "Pi",
            "Rho", "Sigma", "Tau", "Upsilon", "Phi", "Chi", "Psi", "Omega" };

    /**
     * Loads a Bitmap image from disk, but correctly sample as to not produce OutOfMemoryExceptions
     * 
     * @param path path of the image on disk
     * @param reqWidth the width of the imageview we want to put it in
     * @param reqHeight the height of the imageview we want to put it in
     * @return the decoded image
     */
    public static Bitmap getSampledBitmapFromPathResource(String path, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    /**
     * Loads an image into an imageview. This is kind of a wrapper around {@link BitmapWorkerTask}
     * 
     * @param panoPath the path to the image on disk
     * @param imageView the imageview where we want to load the image into
     */
    public static void loadBitmap(String panoPath, ImageView imageView) {
        BitmapWorkerTask task = new BitmapWorkerTask(imageView);
        task.execute(panoPath);
    }
    
    /**
     * compares two points
     *  
     * @param a first point
     * @param b second point
     * @return whether the two point have the same latitude and longitude
     */
    public static boolean equalsLatLng(LatLng a, LatLng b) {
        return a.latitude == b.latitude && a.longitude == b.longitude;
    }
}