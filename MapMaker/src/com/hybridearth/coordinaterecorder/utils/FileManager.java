/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.objects.MapMakerMarker;
import com.hybridearth.coordinaterecorder.objects.MapOverlay;
import com.hybridearth.coordinaterecorder.objects.MetaioMarker;
import com.hybridearth.coordinaterecorder.objects.PanoramaMarker;

/**
 * Takes care of writing to file the lists of objects used in the application
 * 
 * For now this class is not used, but it might turn out useful in case an offline mode is envisioned, for example 
 *  
 * @author triponez
 * @author sciarcia
 */
public class FileManager<T extends MapMakerMarker> {

    private static final String TAG                     = "FileManager";
    private Gson                mGson;

    private Context             mContext;

    public FileManager(Context context) {
        // create a File object for the parent directory
        mGson = new Gson();
        mContext = context;
    }

    /**
     * Write to file a list of objects
     * 
     * @param markers this can be a list of Markers/Panoramas/Overlays. 
     * Since the type of the class is <code><T extends MapMakerMarker></code> we don't care what we're passed, as long as it's a subclass of {@link MapMakerMarker}
     * 
     * @return true if the write operation was successful
     */
    public boolean saveMarkers(List<T> markers) {
        boolean success = false;
        if(markers == null || markers.isEmpty()) {
            return success;
        }
        Type t = null;
        String file = null;
        MapMakerMarker mmm = markers.get(0);
        if(mmm instanceof MetaioMarker) {
            t = new TypeToken<List<MetaioMarker>>(){
                    }.getType();
            file = Consts.MARKERPICS;
        } else if(mmm instanceof PanoramaMarker) {
            t = new TypeToken<List<PanoramaMarker>>(){
                    }.getType();
            file = Consts.PANOS;
        } else if(mmm instanceof MapOverlay) {
            t = new TypeToken<List<MapOverlay>>(){
                    }.getType();
            file = Consts.MAPSPICS;
        } else {
            return success;
        }
        OutputStream fos = null;
        try {
            fos = new BufferedOutputStream(mContext.openFileOutput(Consts.MAP_MAKER + "_" + file, Context.MODE_PRIVATE));
            String json = mGson.toJson(markers, t);
            fos.write(json.getBytes());
            success = true;
        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    Log.e(TAG, "can't close stream - " + e.getMessage());
                }
            }
        }
        return success;
    }
    
    /**
     * Retrieves the list of Markers stored on file
     * 
     * @return the list of Markers stored on file or an empty list if an error occured
     */
    public List<MetaioMarker> getMetaioMarkers() {
        List<MetaioMarker> markers = new ArrayList<MetaioMarker>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(mContext.openFileInput(Consts.MAP_MAKER + "_" + Consts.MARKERPICS)));
            markers = mGson.fromJson(reader, new TypeToken<List<MetaioMarker>>(){
                                                    }.getType());
        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(TAG, "can't close stream - " + e.getMessage());
                }
            }
        }
        return markers;
    }

    /**
     * Retrieves the list of Panoramas stored on file
     * 
     * @return the list of Panoramas stored on file or an empty list if an error occured
     */
    public List<PanoramaMarker> getPanoramaMarkers() {
        List<PanoramaMarker> markers = new ArrayList<PanoramaMarker>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(mContext.openFileInput(Consts.MAP_MAKER + "_" + Consts.PANOS)));
            markers = mGson.fromJson(reader, new TypeToken<List<PanoramaMarker>>(){
                                                    }.getType());
        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(TAG, "can't close stream - " + e.getMessage());
                }
            }
        }
        return markers;
    }

    /**
     * Retrieves the list of Overlays stored on file
     * 
     * @return the list of Overlays stored on file or an empty list if an error occured
     */
    public List<MapOverlay> getMapOverlays() {
        List<MapOverlay> markers = new ArrayList<MapOverlay>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(mContext.openFileInput(Consts.MAP_MAKER + "_" + Consts.MAPSPICS)));
            markers = mGson.fromJson(reader, new TypeToken<List<MapOverlay>>(){
                                                    }.getType());
        } catch (FileNotFoundException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(TAG, "can't close stream - " + e.getMessage());
                }
            }
        }
        return markers;
    }
    
    /**
     * removes any file we might have stored previously
     * 
     * @return if the deletion operation completed successfully
     */
    public boolean clearFiles() {       
        return 
                //overlays
                mContext.deleteFile(Consts.MAP_MAKER + "_" + Consts.MAPSPICS) &&
                //panoramas
                mContext.deleteFile(Consts.MAP_MAKER + "_" + Consts.PANOS) &&
                //markers
                mContext.deleteFile(Consts.MAP_MAKER + "_" + Consts.MARKERPICS);
    }
}
