/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.utils;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.hybridearth.coordinaterecorder.Consts;

/**
 * Takes care of all that is Location.
 * init, connect, disconnect, new position data, etc...
 * 
 * @author sciarcia
 * 
 */
public class MapMakerLocationManager implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    private static final String TAG = "MapMakerLocationManager";

    private LocationClient      mLocationClient;
    private LocationRequest     mLocationRequest;
    private Location            mCurrentLocation;
    private Context             mCtx;

    public MapMakerLocationManager(Context caller) {
        mCtx = caller;
        initLocation();
    }

    /**
     * Creates a new LocationClient to communicate with the system and sets it up with the correct parameters
     */
    private void initLocation() {
        mLocationClient = new LocationClient(mCtx, this, this);
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(Consts.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(Consts.FASTEST_INTERVAL);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult((Activity)mCtx, Consts.CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, e.getMessage());
            }
        } else {
            Toast.makeText(mCtx, "error " + connectionResult.getErrorCode(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mCurrentLocation = mLocationClient.getLastLocation();
        mLocationClient.requestLocationUpdates(mLocationRequest, this);
    }

    @Override
    public void onDisconnected() {
        //do nothing
    }
    
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
    }

    /**
     * @return the LocationClient
     */
    public LocationClient getLocationClient() {
        return mLocationClient;
    }

    /**
     * @return the LocationRequest
     */
    public LocationRequest getLocationRequest() {
        return mLocationRequest;
    }

    /**
     * 
     * @return the device's current location
     */
    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

}
