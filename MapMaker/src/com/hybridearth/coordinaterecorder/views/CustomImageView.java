/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.utils.Log;

/**
 * @author triponez
 * @author sciarcia
 * 
 */
public class CustomImageView extends ImageView {

    private static final String   TAG = "CustomImageView";
    private Paint                 mLinePaint;
    private Paint                 mArrowPaint;
    private float                 mLinePosition;
    private double                mAngle;
    private OnAngleChangeListener mListener;

    public CustomImageView(Context context) {
        super(context);
        init();
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    int finalHeight, finalWidth;

    private void init() {
        ViewTreeObserver vto = this.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                finalHeight = CustomImageView.this.getMeasuredHeight();
                finalWidth = CustomImageView.this.getMeasuredWidth();
                return true;
            }
        });

        mLinePosition = 50;

        mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaint.setStrokeWidth(4);
        mLinePaint.setStyle(Paint.Style.STROKE);
        mLinePaint.setColor(Color.RED);

        mArrowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mArrowPaint.setStyle(Paint.Style.FILL);
        mArrowPaint.setStrokeWidth(4);
        mArrowPaint.setColor(Color.RED);
    }

    public void setAngle(double angle) {
        this.mAngle = angle;
        this.mLinePosition = (float) getAngleFromScreen(angle);
    }

    public void setLinePosition(int position) {
        this.mLinePosition = position;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(mLinePosition, (canvas.getHeight() - actH) / 2, mLinePosition, ((canvas.getHeight() - actH) / 2) + actH, mLinePaint);

        // create and draw triangles
        // use a Path object to store the 3 line segments
        // use .offset to draw in many locations
        // note: this triangle is not centered at 0,0
        Path pathTop = new Path();
        float trianglePosition = mLinePosition;
        pathTop.lineTo(trianglePosition + 15, 0);
        pathTop.lineTo(trianglePosition - 15, 0);
        pathTop.close();
        pathTop.offset(0, 30);
        canvas.drawPath(pathTop, mArrowPaint);

        Path pathBottom = new Path();
        float trianglePositionBottom = mLinePosition;
        pathBottom.lineTo(trianglePositionBottom + 15, 0);
        pathBottom.lineTo(trianglePositionBottom - 15, 0);
        pathBottom.close();
        pathBottom.offset(0, actH);
        canvas.drawPath(pathBottom, mArrowPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.

        float x = e.getX();

        Log.d(TAG, e.getAction() + "");

        switch (e.getAction()) {
            case MotionEvent.ACTION_MOVE:

                mLinePosition = x;
                this.postInvalidate();
                if (mListener != null) {
                    mListener.onPositionChange(this, getAngleFromScreen(mLinePosition));
                }
                break;
            default:
                Log.d(TAG, Consts.UNKNOWN_OPTION);
                break;

        }
        return true;
    }

    public static double roundToTwoPlaces(double d) {
        return Math.floor(100 * d + 0.5) / 100;

    }

    int actW;
    int actH;

    public double getAngleFromScreen(double position) {
        if (actW != 0) {
            return roundToTwoPlaces(((position / actW) * 360) - 180);
        } else {
            return 0;
        }
    }

    public double getPositionFromAngle(double angle) {
        double middle = actW / 2;
        double position = (angle / 360) * actW;
        return middle + position;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // Get image matrix values and place them in an array
        float[] f = new float[9];
        getImageMatrix().getValues(f);

        // Extract the scale values using the constants (if aspect ratio
        // maintained, scaleX == scaleY)
        final float scaleX = f[Matrix.MSCALE_X];
        final float scaleY = f[Matrix.MSCALE_Y];

        // Get the drawable (could also get the bitmap behind the drawable and
        // getWidth/getHeight)
        final Drawable d = getDrawable();
        if(d != null) {
            final int origW = d.getIntrinsicWidth();
            final int origH = d.getIntrinsicHeight();
    
            // Calculate the actual dimensions
            actW = Math.round(origW * scaleX);
            actH = Math.round(origH * scaleY);
            Log.d(TAG, "[" + origW + "," + origH + "] -> [" + actW + "," + actH + "] & scales: x=" + scaleX + " y=" + scaleY);
        }

        mLinePosition = (float) getPositionFromAngle(mAngle);
    }

    public void setOnAngleChangeListener(OnAngleChangeListener listener) {
        mListener = listener;
    }

    public interface OnAngleChangeListener {

        /**
         * On progress change.
         * 
         * @param view
         *            the view
         * @param newProgress
         *            the new progress
         */
        void onPositionChange(CustomImageView view, double newAngle);
    }

}