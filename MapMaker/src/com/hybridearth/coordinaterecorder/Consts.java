/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder;

/**
 * 
 * @author sciarcia
 * 
 */
public class Consts {

    private Consts() {
        // Exists only to defeat instantiation.
    }

    // Server communication parameters
    public static final String PICTURE_EXTRA                         = "picturePath";
    public static final String WEB_SERVER_ADDRESS                    = "hybridearth.net";
//     public static final String WEB_SERVER_ADDRESS = "192.168.1.4";
    public static final String WEB_SERVER_PROTOCOL                   = "http://";
    public static final String WEB_SERVER_URL_NOPORT                 = WEB_SERVER_PROTOCOL + WEB_SERVER_ADDRESS;
    public static final String MAP_MAKER                             = "MapMaker";
    public static final String IDMARKERS_SERVICE                     = "markers";
    public static final String PANORAMA_SERVICE                      = "panoramas";
    public static final String MAPS_SERVICE                          = "mapoverlays";
    public static final String MARKERPICS                            = "markerpics";
    public static final String PANO_IMAGE                            = "panoimage";
    public static final String MAPSPICS                              = "maps";
    public static final String THUMBS                                = "thumbs/";

    //
    public static final String ID                                    = "id";
    public static final String PANOS                                 = PANORAMA_SERVICE;
    public static final String POSITION                              = "position";
    public static final String NEIGHBORS                             = "neighbors";
    public static final String ALTITUDE                              = "altitude";
    public static final String LATITUDE                              = "latitude";
    public static final String LONGITUDE                             = "longitude";
    public static final String LAT                                   = "lat";
    public static final String LON                                   = "lon";
    public static final String ANGLE                                 = "angle";
    public static final String ANGLE_OFFSET                          = "angleOffset";
    public static final String STATUS                                = "status";
    public static final String PATH                                  = "path";
    public static final String THUMBPATH                             = "thumbpath";
    public static final String PITCH                                 = "pitch";
    public static final String CAMERA_ALT                            = "cameraAlt";
    public static final String RELOAD                                = "reload";
    public static final String WIDTH                                 = "width";
    public static final String HEIGHT                                = "height";
    public static final String METAIO_ID                             = "metaioid";
    public static final String ANCHOR                                = "anchor";
    public static final String BEARING                               = "mapBearing";
    public static final String PANO_PATH                             = "panoPath";
    public static final String MARKER_ID                             = "markerID";

    //
    public static final String CAMERA_IMAGE_URI                      = "cameraImageUri";

    /** Keep track of external intents */
    public static final int    CAMERA_PIC_REQUEST                    = 0;
    public static final int    GALLERY_PIC_REQUEST                   = 1;
    public static final int    GALLERY_PIC_REQUEST_DIALOG            = 2;
    public static final int    CROP_PIC_REQUEST                      = 3;
    public static final int    PANO_OFFSET_REQUEST                   = 4;

    public static final String NULL_RESPONSE                         = "null response";
    public static final String NULL_MESSAGE                          = "logging null message";
    public static final String UNKNOWN_TASK                          = "unknown task";
    public static final String UNKNOWN_OPTION                        = "unknown option";

    /** login */
    public static final String STATE                                 = "state";
    public static final String PREF_LOGGED_IN                        = "logged_in";
    public static final String SUCCESS                               = "success";
    public static final String ERROR                                 = "error";
    public static final String SESSION_ID                            = "sessionId";
    public static final int    DO_LOGIN                              = 101;
    public static final int    DO_LOGOUT                             = 102;
    public static final int    DO_SIGNUP                             = 103;

    /** preferences */
    public static final String PREF_FILE                             = "com.hybridearth.coordinaterecorder.preferences";
    public static final String PREF_NAME                             = "prefName";
    public static final String PREF_MAIL                             = "prefMail";
    public static final String PREF_REMEMBER_DATA                    = "prefRememberData";
    public static final String PREF_TEMP_PWD                         = "prefPasswordChange";
    public static final String PREF_PWD                              = "prefPassword";
    public static final String PREF_TEMP_NAME                        = "prefNameChange";
    public static final String ANON_USER                             = "anonymous";
    public static final String USER_TOKEN                            = "userToken";
    public static final String LIMIT                                 = "limit";
    public static final int    LIMIT_DEFAULT                         = 25;
    public static final String SUPER                                 = "super";
    public static final String EDITABLE                              = "editable";
    public static final String OWNER                                 = "owner";
    public static final String OWNER_EMAIL                           = "ownerEmail";

    public static final int    UPDATE_INTERVAL                       = 1000 * 5;
    public static final int    FASTEST_INTERVAL                      = 1000 * 1;

    public static final int    CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    public static final String UTF8                                  = "UTF-8";
}
