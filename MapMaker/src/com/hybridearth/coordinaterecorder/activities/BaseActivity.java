/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Pair;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.MapMakerApplication.AppState;
import com.hybridearth.coordinaterecorder.R;
import com.hybridearth.coordinaterecorder.objects.MapMakerMarker;
import com.hybridearth.coordinaterecorder.objects.MapOverlay;
import com.hybridearth.coordinaterecorder.objects.MetaioMarker;
import com.hybridearth.coordinaterecorder.objects.PanoramaMarker;
import com.hybridearth.coordinaterecorder.server.DownloadManager;
import com.hybridearth.coordinaterecorder.server.DownloadManager.NetworkTransferListener;
import com.hybridearth.coordinaterecorder.utils.FileManager;
import com.hybridearth.coordinaterecorder.utils.Log;
import com.hybridearth.coordinaterecorder.utils.MapMakerLocationManager;
import com.hybridearth.coordinaterecorder.utils.Utils;
import com.hybridearth.coordinaterecorder.utils.Utils.Task;
import com.hybridearth.coordinaterecorder.views.CircularSeekBar;
import com.hybridearth.coordinaterecorder.views.CircularSeekBar.OnSeekChangeListener;

/**
 * Base abstract class holding most of the common functionality.
 * As of now (04-Dec-2013) there are only two subclasses: PanosActivity and MarkersActivity
 * 
 * @author sciarcia
 */
public abstract class BaseActivity extends Activity implements NetworkTransferListener, OnMarkerDragListener {

    private static final String                            TAG              = "BaseActivity";
    /** holds a list of point on the map */
    private LatLngBounds.Builder                           mBc              = new LatLngBounds.Builder();

    protected MapMakerMarker                               mSelectedMarker;
    /** list of markers on the map */
    protected List<MetaioMarker>                           mMarkers;
    /** list of panoramas on the map */
    protected List<PanoramaMarker>                         mPanos;
    /** list of overlays on the map */
    protected List<MapOverlay>                             mOverlays;
    /** Map to translate GoogleMarkers on the map to MapMaker-Markers */
    protected Map<Marker, MapMakerMarker>                  mMarkersTranslationMap;
    /** Map to translate GoogleMarkers on the map to the pair Marker-GroundOverlay */
    protected Map<Marker, Pair<GroundOverlay, MapOverlay>> mOverlaysTranslationMap;
    protected ImageView                                    mPicImageView    = null;
    protected ProgressBar                                  mSpinner         = null;
    protected Button                                       mPanoAngleOffset = null;

    /** list of deleted panoramas to send to server */
    protected List<PanoramaMarker> mDeletedPanoramas = new ArrayList<PanoramaMarker>();
    /** list of newly added panoramas to send to server */
    protected List<PanoramaMarker> mAddedPanoramas = new ArrayList<PanoramaMarker>();
    /** list of modified panoramas to send to server */
    protected List<PanoramaMarker> mModifiedPanoramas = new ArrayList<PanoramaMarker>();
    /** list of deleted markers to send to server */
    protected List<MetaioMarker> mDeletedMarkers = new ArrayList<MetaioMarker>();
    /** list of newly added markers to send to server */
    protected List<MetaioMarker> mAddedMarkers = new ArrayList<MetaioMarker>();
    /** list of modified markers to send to server */
    protected List<MetaioMarker> mModifiedMarkers = new ArrayList<MetaioMarker>();
    /** list of deleted overlays to send to server */
    protected List<MapOverlay> mDeletedOverlays = new ArrayList<MapOverlay>();
    /** list of newly added overlays to send to server */
    protected List<MapOverlay> mAddedOverlays = new ArrayList<MapOverlay>();
    /** list of modified markers to send to server */
    protected List<MapOverlay> mModifiedOverlays = new ArrayList<MapOverlay>();
    
    /***************************************************************
     * Lifecycle stuff
     ***************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_map);

        mMarkers = new ArrayList<MetaioMarker>();
        mPanos = new ArrayList<PanoramaMarker>();
        mOverlays = new ArrayList<MapOverlay>();
        mMarkersTranslationMap = new HashMap<Marker, MapMakerMarker>();
        mOverlaysTranslationMap = new HashMap<Marker, Pair<GroundOverlay, MapOverlay>>();
        mProgressDialog = Utils.initDialog(this);
        setupViews();
    }

    private void setupViews() {
        Button save = (Button) findViewById(R.id.saveButton);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSave();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent returnedIntent) {
        super.onActivityResult(requestCode, resultCode, returnedIntent);
        switch (requestCode) {
            case Consts.CAMERA_PIC_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (returnedIntent != null) {
                        mImageUri = returnedIntent.getData();
                    }
                    Utils.performCrop(this, mImageUri);
                }
                break;
            case Consts.GALLERY_PIC_REQUEST:
                if (resultCode == RESULT_OK) {
                    mImageUri = returnedIntent.getData();
                    Utils.performCrop(this, mImageUri);
                }
                break;
            case Consts.CONNECTION_FAILURE_RESOLUTION_REQUEST:
                Log.d(TAG, "should be trying to connect again");
                break;
            case Consts.GALLERY_PIC_REQUEST_DIALOG:
                if (resultCode == RESULT_OK) {
                    Uri returned = returnedIntent.getData();
                    setPanorama(Utils.getRealPathFromURI(returned));
                }
                break;
            case Consts.PANO_OFFSET_REQUEST:
                if (resultCode == RESULT_OK) {
                    String markerID = returnedIntent.getExtras().getString(Consts.MARKER_ID);
                    double angleOffset = returnedIntent.getExtras().getDouble(Consts.ANGLE_OFFSET);
                    Log.d(TAG, "Modified: " + markerID);
                    updateMarkerOffset(markerID, angleOffset);
                }
                break;
            default:
                // do nothing
                Log.d(TAG, Consts.UNKNOWN_OPTION);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        MapMakerApplication.getLocationManager().getLocationClient().connect();
    }

    @Override
    protected void onStop() {
        MapMakerLocationManager lm = MapMakerApplication.getLocationManager();
        if (lm.getLocationClient().isConnected()) {
            lm.getLocationClient().removeLocationUpdates(lm);
        }
        lm.getLocationClient().disconnect();
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mProgressDialog != null) {
            hideDialog();
        }
    }

    @Override
    public void onBackPressed() {
        switch (MapMakerApplication.getAppState()) {
            case EDIT:
                if (mAngleEditing) {
                    mMap.getUiSettings().setAllGesturesEnabled(true);
                    mMap.setMyLocationEnabled(true);
                    ((LinearLayout) findViewById(R.id.angle_layout)).setVisibility(View.GONE);
                    circularSeekbar.setVisibility(View.GONE);
                    mAngleEditing = false;
                } else {
                    Toast.makeText(this, "exit edit mode", Toast.LENGTH_SHORT).show();
                    MapMakerApplication.setAppState(AppState.LOGGED_IN);
                    invalidateOptionsMenu();
                }
                break;
            default:
                if(!confirmChangesOnExit()){
                    super.onBackPressed();
                }
                break;
        }
    }

    /***************************************************************
     * ProgressDialog stuff
     ***************************************************************/

    /** Upload and download progress dialog */
    protected ProgressDialog mProgressDialog;

    /** show the progress dialog */
    protected void showDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    /**
     * set the progress level of the dialog
     * 
     * @param progress
     */
    protected void setDialogProgress(int progress) {
        if (mProgressDialog != null) {
            mProgressDialog.setProgress(progress);
        }
    }

    /** hide the progress dialog */
    protected void hideDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
        }
    }

    /***************************************************************
     * MapMakerMarker-angle stuff
     ***************************************************************/

    /** Circular seek bar the user can drag, for panorama angle */
    protected CircularSeekBar circularSeekbar;
    /** 
     * flag used to know if we're editing the angle of a marker/panorama
     * We use this because if this is the case additional objects are shown on the screen 
     * (save/cancel buttons, the circularseekbar...) 
     */
    private boolean           mAngleEditing = false;

    /**
     * Shows the edit angle view: the map is centered on the marker for which we
     * want to edit the panorama angle and the user can set a new angle
     * 
     * @param angle
     *            the current angle value for the selected
     *            <code>MapMakerMarker</code>
     */
    protected void editAngle(double angle) {
        mAngleEditing = true;
        ((LinearLayout) findViewById(R.id.angle_layout)).setVisibility(View.VISIBLE);
        final TextView angleValue = (TextView) findViewById(R.id.value_angle);
        angleValue.setText(angle + "");

        circularSeekbar = (CircularSeekBar) findViewById(R.id.circularseekbar);
        circularSeekbar.setMaxProgress(360);
        circularSeekbar.setAngle((int) angle);
        circularSeekbar.setProgress((int) angle);

        circularSeekbar.invalidate();
        circularSeekbar.setSeekBarChangeListener(new OnSeekChangeListener() {
            @Override
            public void onProgressChange(CircularSeekBar view, int newProgress) {
                angleValue.setText(newProgress + "");
            }
        });

        circularSeekbar.setVisibility(View.VISIBLE);
    }

    /**
     * Centers the map to the selected marker/panorama and disables map actions to 
     * make angle mofifications smoother
     * 
     * @param marker the marker/panorama whose angle we're modifying
     */
    protected void enableAngle(MapMakerMarker marker) {
        // Disable map actions so that the user can only zoom with the buttons
        mMap.setOnMapClickListener(null);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMyLocationEnabled(false);
        // Sets the center of the map to
        CameraPosition cameraPosition = new CameraPosition.Builder().target(marker.getPosition())
        // Sets the zoom
                .zoom(mMap.getCameraPosition().zoom)
                // Sets the orientation of the camera to east
                .bearing(0)
                // Creates a CameraPosition from the builder
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    /**
     * Re enables map controls and save the modified angle
     * 
     * @param angle the new angle to set on the marker/panorama
     */
    protected void saveAngle(int angle) {
        // Re-enable map controls
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.setMyLocationEnabled(true);

        if(angle != mSelectedMarker.getAngle()) {
            mSelectedMarker.setAngle(angle);
            if(mSelectedMarker instanceof PanoramaMarker){
                if(mAddedPanoramas.contains((PanoramaMarker)mSelectedMarker)){
                    //we just added this, do nothing
                } else if(!mModifiedPanoramas.contains((PanoramaMarker)mSelectedMarker)){
                    mModifiedPanoramas.add((PanoramaMarker)mSelectedMarker);
                }
            } else if(mSelectedMarker instanceof MetaioMarker){
                if(mAddedMarkers.contains((MetaioMarker)mSelectedMarker)){
                    //we just added this, do nothing
                } else if(!mModifiedMarkers.contains((MetaioMarker)mSelectedMarker)){
                    mModifiedMarkers.add((MetaioMarker)mSelectedMarker);
                }
            }
        }
        mAngleEditing = false;
    }

    /***************************************************************
     * Map-related stuff
     ***************************************************************/

    /** Google Map object */
    protected GoogleMap mMap;

    /** Initialize google maps. */
    protected boolean initMap() {
        try {
            MapsInitializer.initialize(this);
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, e.getMessage());
        }

        if (mMap == null) {
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.googlemap)).getMap();
            if (mMap != null) {
                setupMap();
            }
        }
        return mMap != null;
    }

    /** 
     * Sets up the map (map type, use location, etc)
     */
    protected void setupMap() {
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.getUiSettings().setTiltGesturesEnabled(false);
        mMap.setMyLocationEnabled(true);
    }

    /** Clear the map from artifacts added to it. */
    protected void clearMap() {
        mMarkersTranslationMap.clear();
        mMap.clear();
    }

    /** Makes the map show all the points drawn on the map */
    private void moveCamera() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        Location l = MapMakerApplication.getLocationManager().getCurrentLocation();
        if (l != null) {
            mBc.include(new LatLng(l.getLatitude(), l.getLongitude()));
        }
        try {
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(mBc.build(), width, height, 50));
        } catch (IllegalStateException e) {
            Log.e(TAG, "no points in LatLng Builder");
        }
    }

    @Override
    public void onMarkerDrag(Marker draggedMarker) {
        // do nothing
    }

    @Override
    public void onMarkerDragEnd(Marker draggedMarker) {
        MapMakerMarker mmm = mMarkersTranslationMap.get(draggedMarker);
        if(!Utils.equalsLatLng(mmm.getPosition(),draggedMarker.getPosition())){
            mmm.setPosition(draggedMarker.getPosition());
            if(mmm instanceof PanoramaMarker){
                if(mAddedPanoramas.contains((PanoramaMarker)mmm)){
                    //we just created this, do nothing 
                 } else if(!mModifiedPanoramas.contains((PanoramaMarker)mmm)) {
                     mModifiedPanoramas.add((PanoramaMarker)mmm);
                 }
            } else if(mmm instanceof MetaioMarker){
                if(mAddedMarkers.contains((MetaioMarker)mmm)){
                    //we just created this, do nothing 
                 } else if(!mModifiedMarkers.contains((MetaioMarker)mmm)) {
                     mModifiedMarkers.add((MetaioMarker)mmm);
                 }
            }
        }
    }

    @Override
    public void onMarkerDragStart(Marker draggedMarker) {
        // do nothing
    }

    /***************************************************************
     * Menu-related stuff
     ***************************************************************/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, PrefsActivity.class));
                return true;
            case R.id.action_logout:
                onClickLogout();
                return true;
            case R.id.action_edit:
                onClickEdit();
                return true;
            case R.id.action_clear:
                onClickClear();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Called when the user pressed the clear button. Prompts the user for
     * delete confirmation; if the user confirms, markers, polylines and
     * {@link GroundOverlay}s will be deleted from the map
     * 
     * @param m
     *            the {@link MenuItem} that was pressed
     */
    protected void onClickClear() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.clear_dialog_title)).setMessage(getResources().getString(R.string.clear_dialog_text))
                .setNegativeButton(android.R.string.no, null).setPositiveButton(android.R.string.yes, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        clearMap();
                        Log.d(TAG, "delete file cache: " + new FileManager(BaseActivity.this).clearFiles());
                    }
                }).create().show();
    }

    /**
     * Called when the user pressed the "upload to server" button. Prompts the
     * user for confirmation. If the user confirms, markers, panoramas and
     * {@link GroundOverlay}s will be uploaded to the server.
     * 
     * @param m
     *            the {@link MenuItem} that was pressed
     */
    protected void onClickUpload(MenuItem m) {
        switch (m.getItemId()) {
            case R.id.action_upload_markers:
            case R.id.action_upload_panos:
                confirmChanges();
                break;
            default:
                // do nothing
                Log.d(TAG, Consts.UNKNOWN_OPTION);
                return;
        }
    }

    private boolean mSth = false;
    
    /**
     * Creates a message to show to the user with the list of modifications to send to the server
     * 
     * @param initial a string to show at the beginning of the message 
     * @return the complete message to show on a dialog
     */
    private String buildConfirmationMessage(String initial) {
        StringBuilder msg = new StringBuilder();
        msg.append(initial);
        mSth = false;
        if (!mDeletedPanoramas.isEmpty()) {
            msg.append("\n" + getString(R.string.bullet) + " " + mDeletedPanoramas.size() + " " + getString(R.string.panoramas) + " " + getString(R.string.deleted));
            mSth = true;
        }
        if (!mAddedPanoramas.isEmpty()) {
            msg.append("\n" + getString(R.string.bullet) + " " + mAddedPanoramas.size() + " " + getString(R.string.panoramas) + " " + getString(R.string.added));
            mSth = true;
        }
        if (!mModifiedPanoramas.isEmpty()) {
            msg.append("\n" + getString(R.string.bullet) + " " + mModifiedPanoramas.size() + " " + getString(R.string.panoramas) + " " + getString(R.string.modified));
            mSth = true;
        }
        if (!mDeletedMarkers.isEmpty()) {
            msg.append("\n" + getString(R.string.bullet) + " " + mDeletedMarkers.size() + " " + getString(R.string.markers) + " " + getString(R.string.deleted));
            mSth = true;
        }
        if (!mAddedMarkers.isEmpty()) {
            msg.append("\n" + getString(R.string.bullet) + " " + mAddedMarkers.size() + " " + getString(R.string.markers) + " " + getString(R.string.added));
            mSth = true;
        }
        if (!mModifiedMarkers.isEmpty()) {
            msg.append("\n" + getString(R.string.bullet) + " " + mModifiedMarkers.size() + " " + getString(R.string.markers) + " " + getString(R.string.modified));
            mSth = true;
        }
        if (!mDeletedOverlays.isEmpty()) {
            msg.append("\n" + getString(R.string.bullet) + " " + mDeletedOverlays.size() + " " + getString(R.string.maps) + " " + getString(R.string.deleted));
            mSth = true;
        }
        if (!mAddedOverlays.isEmpty()) {
            msg.append("\n" + getString(R.string.bullet) + " " + mAddedOverlays.size() + " " + getString(R.string.maps) + " " + getString(R.string.added));
            mSth = true;
        }
        if (!mModifiedOverlays.isEmpty()) {
            msg.append("\n" + getString(R.string.bullet) + " " + mModifiedOverlays.size() + " " + getString(R.string.maps) + " " + getString(R.string.modified));
            mSth = true;
        }
        return msg.toString();
    }
    
    /**
     * Shows a dialog to the user asking to send the changes to the server or not
     */
    private void confirmChanges() {
        String msg = buildConfirmationMessage(getString(R.string.changes_to_upload));
        if(mSth){
            new AlertDialog.Builder(this).setTitle(getString(R.string.review_changes)).setMessage(msg).setNegativeButton(android.R.string.cancel, null)
                    .setPositiveButton(android.R.string.yes, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            launchUploadingTasks();
                        }
                    }).create().show();
        } else {
            Toast.makeText(this, getString(R.string.no_changes), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * shows a dialog to the user if he's made some modifications but hasn't sent them to the server
     * Also see {@link BaseActivity#confirmChanges()}
     * 
     * @return true if there's something to upload
     */
    private boolean confirmChangesOnExit() {
        String msg = buildConfirmationMessage(getString(R.string.send_before_exit));
        if(mSth){
            new AlertDialog.Builder(this).setTitle(getString(R.string.review_changes)).setMessage(msg)
                    .setNegativeButton(android.R.string.no, new OnClickListener(){
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            BaseActivity.this.finish();
                        }
                    })
                    .setPositiveButton(android.R.string.yes, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            launchUploadingTasks();
                        }
                    }).create().show();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Starts the uploading of data to the server.
     */
    private void launchUploadingTasks() {
        if(!mAddedMarkers.isEmpty()){
            new DownloadManager<MetaioMarker>(BaseActivity.this, mAddedMarkers).execute(Task.UPLOAD_MARKERS);
        }
        if(!mModifiedMarkers.isEmpty()){
            new DownloadManager<MetaioMarker>(BaseActivity.this, mModifiedMarkers).execute(Task.UPLOAD_MARKERS);
        }
        if(!mDeletedMarkers.isEmpty()){
            new DownloadManager<MetaioMarker>(BaseActivity.this, mDeletedMarkers).execute(Task.UPLOAD_MARKERS);
        }
        if(!mAddedPanoramas.isEmpty()){
            new DownloadManager<PanoramaMarker>(BaseActivity.this, mAddedPanoramas).execute(Task.UPLOAD_PANOS);
        }
        if(!mModifiedPanoramas.isEmpty()){
            new DownloadManager<PanoramaMarker>(BaseActivity.this, mModifiedPanoramas).execute(Task.UPLOAD_PANOS);
        }
        if(!mDeletedPanoramas.isEmpty()){
            new DownloadManager<PanoramaMarker>(BaseActivity.this, mDeletedPanoramas).execute(Task.UPLOAD_PANOS);
        }
        if(!mAddedOverlays.isEmpty()){
            new DownloadManager<MapOverlay>(BaseActivity.this, mAddedOverlays).execute(Task.UPLOAD_OVERLAYS);
        }
        if(!mModifiedOverlays.isEmpty()){
            new DownloadManager<MapOverlay>(BaseActivity.this, mModifiedOverlays).execute(Task.UPLOAD_OVERLAYS);
        }
        if(!mDeletedOverlays.isEmpty()){
            new DownloadManager<MapOverlay>(BaseActivity.this, mDeletedOverlays).execute(Task.UPLOAD_OVERLAYS);
        }
    }
    
    /**
     * Once we sent our data to the server we reset all modifications tracking to start fresh
     */
    protected void resetChanges() {
        if(!MapMakerApplication.getInstance().isDownloading()){
            mPanos.addAll(mAddedPanoramas);
            for(PanoramaMarker p : mDeletedPanoramas) {
                mPanos.remove(p);
            }
            mAddedPanoramas.clear();
            mDeletedPanoramas.clear();
            mModifiedPanoramas.clear();
            mMarkers.addAll(mAddedMarkers);
            for(MetaioMarker m : mDeletedMarkers) {
                mMarkers.remove(m);
            }
            mAddedMarkers.clear();
            mDeletedMarkers.clear();
            mModifiedMarkers.clear();
            mOverlays.addAll(mAddedOverlays);
            for(MapOverlay o : mDeletedOverlays) {
                mOverlays.remove(o);
            }
            mAddedOverlays.clear();
            mDeletedOverlays.clear();
            mModifiedOverlays.clear();
            mSth = false;
        }
    }

    /**
     * Enters the application in EDIT mode. See {@link AppState}
     */
    protected void onClickEdit() {
        switch (MapMakerApplication.getAppState()) {
            case EDIT:
                Toast.makeText(this, getString(R.string.exit_edit_mode), Toast.LENGTH_SHORT).show();
                changeMarkersDraggableState(false);
                MapMakerApplication.setAppState(AppState.LOGGED_IN);
                invalidateOptionsMenu();
                break;
            case LOGGED_IN:
                Toast.makeText(this, getString(R.string.enter_edit_mode), Toast.LENGTH_SHORT).show();
                changeMarkersDraggableState(true);
                MapMakerApplication.setAppState(AppState.EDIT);
                invalidateOptionsMenu();
                break;
            default:
                // do nothing
                Log.d(TAG, Consts.UNKNOWN_OPTION);
                break;
        }
    }

    /**
     * Sets the draggable state of the markers on the map
     * @param draggable whether to make the markers draggable or not
     */
    private void changeMarkersDraggableState(boolean draggable) {
        for (Marker m : mMarkersTranslationMap.keySet()) {
            m.setDraggable(draggable);
        }
    }

    /**
     * Performs logout
     */
    protected void onClickLogout() {
        Utils.wipeUserData();
        finish();
    }

    /**
     * Called when the user presses the add picture button of the MapMakerMarker
     * dialog. Starts an activity for the user to pick a picture to associate
     * with the current marker
     */
    protected void onClickAddPicture() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, Consts.GALLERY_PIC_REQUEST_DIALOG);
    }

    /**
     * Called when the user pressed the save button on the edit angle
     * {@link View}
     * 
     * @param v
     *            the clicked view
     */
    protected void onClickSave() {
        int progress = circularSeekbar.getProgress();
        saveAngle(progress);
        circularSeekbar.setVisibility(View.GONE);
        circularSeekbar.setProgress(0);
        ((LinearLayout) findViewById(R.id.angle_layout)).setVisibility(View.GONE);
    }

    /***************************************************************
     * NetworkTransferListener operations
     ***************************************************************/

    @Override
    public void onPanoramaMarkersDownloaded(List<PanoramaMarker> panos) {
        if (panos != null) {
            mPanos = panos;
            // show markers on map
            for (PanoramaMarker m : mPanos) {
                Marker mapMarker = mMap.addMarker(new MarkerOptions().position(m.getPosition()));
                Log.d(TAG, "added " + m.getType() + " panorama at " + m.getPosition());
                switch (m.getType()) {
                    case INNER:
                        mapMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
                        break;
                    case ENTRANCE:
                        mapMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                        break;
                    default:
                        // do nothing
                        Log.e(TAG, Consts.UNKNOWN_OPTION);
                        break;
                }
                mMarkersTranslationMap.put(mapMarker, m);
                mBc.include(m.getPosition());
            }
        } else {
            mPanos = new ArrayList<PanoramaMarker>();
        }
        moveCamera();
    }

    @Override
    public void onMetaioMarkersDownloaded(List<MetaioMarker> markers) {
        if (markers != null) {
            mMarkers = markers;
            // show markers on map
            for (MetaioMarker m : mMarkers) {
                Marker mapMarker = mMap.addMarker(new MarkerOptions().position(m.getPosition()));
                mMarkersTranslationMap.put(mapMarker, m);
                mBc.include(m.getPosition());
            }
        } else {
            mMarkers = new ArrayList<MetaioMarker>();
        }
        moveCamera();
    }

    @Override
    public void onOverlaysDownloaded(List<MapOverlay> overlays) {
        if (overlays != null) {
            mOverlays = overlays;
            Log.d(TAG, "downloaded " + overlays.size() + " overlays from server");
            // show overlays to the map
            for (MapOverlay o : mOverlays) {
                mBc.include(o.getPosition());
                GroundOverlayOptions goo = new GroundOverlayOptions().anchor(0.5f, 0.5f).position(o.getPosition(), o.getWidth(), o.getHeight()).bearing(o.getBearing());
                if (o.getPicPath() != null) {
                    BitmapDescriptor bd = BitmapDescriptorFactory.fromPath(o.getPicPath());
                    goo.image(bd);
                    GroundOverlay go = mMap.addGroundOverlay(goo);
                    Marker key = mMap.addMarker(new MarkerOptions().position(o.getPosition()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
                    key.setVisible(false);
                    mOverlaysTranslationMap.put(key, Pair.create(go, o));
                } else {
                    Log.e(TAG, "no image provided for overlay");
                }
            }
        } else {
            mOverlays = new ArrayList<MapOverlay>();
        }
        moveCamera();
    }

    @Override
    public void onImageDowloaded(MapMakerMarker marker, String imgPath) {
        marker.setPicPath(imgPath);
    }

    @Override
    public void onMarkersUploaded() {
        for(int i = mOverlays.size()-1;i >= 0;i--) {
            MapOverlay o = mOverlays.get(i);
            if (o.isDelete()) {
                mOverlays.remove(i);
            }
        }
        resetChanges();
    }

    /***************************************************************
     * Image stuff
     ***************************************************************/

    /** Uri to an image */
    protected Uri mImageUri;

    /**
     * Clicking on a marker opens a dialog with the details. In there there is a
     * button to add an image to the marker, choosing it from the gallery. Once
     * the use chooses the image we set the preview in the dialog here.
     * 
     * @param returnedImage
     *            the path to the image to set
     */
    public void setPanorama(String returnedImage) {
        mSelectedMarker.setPicPath(returnedImage);
        mSelectedMarker.setChangedPicture(true);
        if(mSelectedMarker instanceof PanoramaMarker) {
            if(mAddedPanoramas.contains((PanoramaMarker)mSelectedMarker)){
                //we just added this, do nothing
            } else if(!mModifiedPanoramas.contains((PanoramaMarker)mSelectedMarker)){
                mModifiedPanoramas.add((PanoramaMarker)mSelectedMarker);
            }
        } else if(mSelectedMarker instanceof MetaioMarker) {
            if(mAddedMarkers.contains((MetaioMarker)mSelectedMarker)){
                //we just added this, do nothing
            } else if(!mModifiedMarkers.contains((MetaioMarker)mSelectedMarker)){
                mModifiedMarkers.add((MetaioMarker)mSelectedMarker);
            }
        }
        Utils.loadBitmap(returnedImage, mPicImageView);
        if (mPanoAngleOffset != null) {
            mPanoAngleOffset.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Coming from the {@link FullScreenActivity} we set a new 'center' for this panorama 
     * with the value provided in that activity.
     * 
     * @param markerID the panorama we're modifying
     * @param angleOffset the new angleOffset for this panorama
     */
    private void updateMarkerOffset(String markerID, double angleOffset) {
        for (PanoramaMarker m : mPanos) {
            if (m.getId().equals(markerID)) {
                m.setPanoCenter(angleOffset);
                break;
            }
        }
    }

}
