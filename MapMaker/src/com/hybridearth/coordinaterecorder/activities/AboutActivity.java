/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.activities;

import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.R;
import com.hybridearth.coordinaterecorder.utils.Log;

/**
 * "About" screen.
 *  
 * @author sciarcia
 */
public class AboutActivity extends Activity {

    private static final String TAG = "AboutActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);

        String appVer = null;
        int appNum = 0;
        try {
            appVer = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
            appNum = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            Log.v(TAG, e.getMessage());
            appVer = getString(R.string.up_to_date);
        }

        TextView version = (TextView) findViewById(R.id.version);
        version.setText(getString(R.string.version) + " " + appVer + " - " + appNum); 
        
        TextView user = (TextView) findViewById(R.id.user);
        String u = MapMakerApplication.getPreferences().getString(Consts.OWNER, null);
        String e = MapMakerApplication.getPreferences().getString(Consts.OWNER_EMAIL, null);
        boolean s = MapMakerApplication.getPreferences().getBoolean(Consts.SUPER, false);
        if(u != null && !"".equals(u)) {
            StringBuilder userMsg = new StringBuilder();
            userMsg.append(getString(R.string.loggedas));
            userMsg.append(" ");
            userMsg.append(u);
            userMsg.append(" (");
            userMsg.append(e);
            userMsg.append(")");
            if(s) {
                userMsg.append("\n---[you are super-user]---");
            }
            user.setText(userMsg.toString());
            user.setVisibility(View.VISIBLE);
        } else {
            user.setVisibility(View.GONE);
        }
        
        TextView visit = (TextView) findViewById(R.id.visit);
        visit.setText(Html.fromHtml(getString(R.string.visit_site)), TextView.BufferType.SPANNABLE);
        visit.setMovementMethod(LinkMovementMethod.getInstance());
        
        Button b = (Button)findViewById(R.id.legal);
        b.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showLegal();
            }
        });
    }

    /**
     * Will show the legal notes for Google Maps.
     * See {@link https://developers.google.com/maps/documentation/android/intro#attribution_requirements} 
     * for more information.
     */
    private void showLegal() {
        TextView legalText = (TextView) findViewById(R.id.legalText);
        Button legal = (Button) findViewById(R.id.legal);
        legal.setVisibility(View.GONE);
        legalText.setText(GooglePlayServicesUtil.getOpenSourceSoftwareLicenseInfo(this));
    }
}
