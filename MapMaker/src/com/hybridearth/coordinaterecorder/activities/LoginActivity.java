/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.R;
import com.hybridearth.coordinaterecorder.server.RegistrationManager;
import com.hybridearth.coordinaterecorder.server.RegistrationManager.OnRegistrationListener;
import com.hybridearth.coordinaterecorder.utils.Utils.Task;

/**
 * Login/Signup screen
 * 
 * @author sciarcia
 */
public class LoginActivity extends Activity implements OnRegistrationListener {

    private static final String TAG     = "LoginActivity";

    /** local reference to sharedPreferences */
    private SharedPreferences   mPrefs;

    private TextView            mHeader;
    private LinearLayout        mSignupLayout;
    private Button              mSigninButton;
    private Button              mSignupButton;

    private CheckBox            mCheckBox;
    private EditText            mMailEditText;
    private EditText            mNameEditText;
    private EditText            mPasswordEditText;
    private String              mEmail;
    private String              mPassword;
    private String              mName;

    /**
     * false=login
     * true=signup
     */
    private boolean             mSignup = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        mPrefs = MapMakerApplication.getPreferences();
        setupViews();
    }

    @Override
    public void onBackPressed() {
        if (mSignup) {
            switchEntries();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * finds views (buttons, edittext, etc) and sets up values and listeners on them
     */
    private void setupViews() {
        mHeader = (TextView) findViewById(R.id.header);
        mSignupLayout = (LinearLayout) findViewById(R.id.signup_layout);
        mSigninButton = (Button) findViewById(R.id.signin_button);
        mSignupButton = (Button) findViewById(R.id.signup_button);
        TextView signupText = (TextView) findViewById(R.id.signupText);

        mNameEditText = (EditText) findViewById(R.id.reg_fullname);
        mMailEditText = (EditText) findViewById(R.id.reg_email);
        mPasswordEditText = (EditText) findViewById(R.id.reg_password);

        mCheckBox = (CheckBox) findViewById(R.id.reg_remember);
        boolean rememberData = mPrefs.getBoolean(Consts.PREF_REMEMBER_DATA, false);
        if (rememberData) {
            mMailEditText.setText(mPrefs.getString(Consts.PREF_MAIL, ""));
        }
        mCheckBox.setChecked(rememberData);
        
        mSigninButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSignin();
            }
        });
        mSignupButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSignup(true);
            }
        });
        signupText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSignup(false);
            }
        });
    }

    /**
     * Retrieves data present in the text fields and preforms a validity check 
     * 
     * @param signup are we operating a signup or a login?
     * @return if it's ok to proceed or not. 
     */
    private boolean getFields(boolean signup) {
        mEmail = mMailEditText.getText().toString().trim();
        mPassword = mPasswordEditText.getText().toString().trim();
        mName = mNameEditText.getText().toString().trim();
        boolean ok = false;

        if (!"".equals(mEmail) && !"".equals(mPassword)) {
            if (signup && !"".equals(mName)) {
                ok = true;
            } else {
                ok = true;
            }
            if (ok) {
                if (mCheckBox.isChecked()) {
                    mPrefs.edit().putString(Consts.PREF_MAIL, mEmail).commit();
                } else {
                    mPrefs.edit().putString(Consts.PREF_MAIL, null).commit();
                }
                mPrefs.edit().putBoolean(Consts.PREF_REMEMBER_DATA, mCheckBox.isChecked()).commit();
            }
        }
        return ok;
    }

    /**
     * shows a toast in case one of the fields is empty
     */
    private void fieldsError() {
        Toast.makeText(this, getString(R.string.cannot_empty), Toast.LENGTH_SHORT).show();
    }

    /**
     * performs the switch between the login and signup screens 
     */
    private void switchEntries() {
        if (!mSignup) {
            // login -> signup
            mHeader.setText(getString(R.string.signup));
            mNameEditText.setVisibility(View.VISIBLE);
            mSignupLayout.setVisibility(View.GONE);
            mSigninButton.setVisibility(View.GONE);
            mSignupButton.setVisibility(View.VISIBLE);
            setTitle(getString(R.string.signup));
        } else {
            // signup -> login
            mHeader.setText(getString(R.string.signin));
            mNameEditText.setVisibility(View.GONE);
            mSignupLayout.setVisibility(View.VISIBLE);
            mSigninButton.setVisibility(View.VISIBLE);
            mSignupButton.setVisibility(View.GONE);
            setTitle(getString(R.string.signin));
        }
        mSignup = !mSignup;
    }

    /**
     * performs a check on the inputs and if everything is ok it continues with the signin operation
     */
    public void onClickSignin() {
        if (!getFields(false)) {
            fieldsError();
        } else {
            Log.d(TAG, "doLogin");
            Toast.makeText(this, "logging in", Toast.LENGTH_SHORT).show();
            new RegistrationManager(this, new String[] { mEmail, mPassword }).execute(Task.LOGIN);
        }
    }

    /**
     * if we're executing a signup performs a check on the inputs and 
     * if everything is ok it continues with the signup operation.
     * Otherwise it means we are just switching to the signup screen
     * 
     * @param exec
     */
    private void onClickSignup(boolean exec) {
        if (exec) {
            if (!getFields(true)) {
                fieldsError();
            } else {
                Log.d(TAG, "doSignup");
                new RegistrationManager(this, new String[] { mEmail, mPassword, mName }).execute(Task.SIGNUP);
            }
        } else {
            switchEntries();
        }
    }

    @Override
    public void onLoginSuccessful(String userId) {
        mPrefs.edit().putString(Consts.USER_TOKEN, userId).commit();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onLoginFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLogoutSuccessful(String userId) {
        // do nothing
    }

    @Override
    public void onLogoutFailed(String error) {
        // do nothing
    }

    @Override
    public void onSignupSuccessful(String userId) {
        mPrefs.edit().putString(Consts.USER_TOKEN, userId).commit();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onSignupFailed(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

}
