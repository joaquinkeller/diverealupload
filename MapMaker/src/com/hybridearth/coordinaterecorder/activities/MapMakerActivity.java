/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.MapMakerApplication.AppState;
import com.hybridearth.coordinaterecorder.R;
import com.hybridearth.coordinaterecorder.server.RegistrationManager;
import com.hybridearth.coordinaterecorder.server.RegistrationManager.OnRegistrationListener;
import com.hybridearth.coordinaterecorder.utils.Utils;
import com.hybridearth.coordinaterecorder.utils.Utils.Task;

/**
 * Application's main {@link Activity}. Simply asks the user to choose between
 * two different map types, panorama and ID MapMakerMarker
 * 
 * @author triponez
 * @author sciarcia
 * 
 */
public class MapMakerActivity extends Activity implements OnRegistrationListener {

    private static final String TAG = "MapMakerActivity";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupViews();
    }

    /**
     * finds views and sets up listeners on them
     */
    public void setupViews() {
        LinearLayout panoramas = (LinearLayout)findViewById(R.id.panoramas);
        panoramas.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPanoramas();
            }
        });
        LinearLayout markers = (LinearLayout)findViewById(R.id.markers);
        markers.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickIDMarkers();
            }
        });
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case Consts.DO_LOGIN:
                if(resultCode == Activity.RESULT_OK){
                    MapMakerApplication.setAppState(AppState.LOGGED_IN);
                    invalidateOptionsMenu();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch(MapMakerApplication.getAppState()){
            case LOGGED_OUT:
                getMenuInflater().inflate(R.menu.main_loggedout, menu);
                return true;
            case LOGGED_IN:
                getMenuInflater().inflate(R.menu.main_loggedin, menu);
                return true;
            default:
                return false;
        }
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, PrefsActivity.class));
                return true;
            case R.id.action_login:
                onClickLogin();
                return true;
            case R.id.action_logout:
                onClickLogout();
                return true;
            case R.id.about:
                onClickAbout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    /**
     * the "login" menu option is selected. launch {@link LoginActivity}
     */
    private void onClickLogin() {
        startActivityForResult(new Intent(this, LoginActivity.class), Consts.DO_LOGIN);
    }

    /**
     * the "logout" menu option is selected, launch logout operation
     */
    private void onClickLogout() {
        Toast.makeText(this, "logging out", Toast.LENGTH_SHORT).show();
        new RegistrationManager(this, new String[]{ MapMakerApplication.getPreferences().getString(Consts.USER_TOKEN, "") }).execute(Task.LOGOUT);
    }
    
    /**
     * user selected to proceed to the {@link PanosActivity}.
     * If he's loggedin we proceed, otherwise we show a toast askin g him to authentify himself
     */
    private void onClickPanoramas() {
        if(MapMakerApplication.getAppState() == AppState.LOGGED_IN) {
                Intent showPanoramas = new Intent(this, PanosActivity.class);
                startActivity(showPanoramas);
        } else {
            Toast.makeText(this, getString(R.string.please_signin), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * user selected to proceed to the {@link MarkersActivity}.
     * If he's loggedin we proceed, otherwise we show a toast askin g him to authentify himself
     */
    private void onClickIDMarkers() {
        if(MapMakerApplication.getAppState() == AppState.LOGGED_IN) {
                Intent showIDMarkers = new Intent(this, MarkersActivity.class);
                startActivity(showIDMarkers);
        } else {
            Toast.makeText(this, getString(R.string.please_signin), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * the "about" menu entry is selected. launch {@link AboutActivity}
     */
    private void onClickAbout() {
        startActivity(new Intent(this, AboutActivity.class));
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
        // check if PlayServices is present
        if(Utils.checkGooglePlayServices(this)){
            // check if GoogleMaps is present
            Utils.checkGoogleMaps(this);
        }
    }

    @Override
    public void onLoginSuccessful(String userId) {
        //do nothing
    }

    @Override
    public void onLoginFailed(String error) {
        Log.e(TAG, "error while logging in: " + error);
        Utils.wipeUserData();
        MapMakerApplication.setAppState(AppState.LOGGED_OUT);
    }

    @Override
    public void onLogoutSuccessful(String userId) {
        Utils.wipeUserData();
        invalidateOptionsMenu();
    }

    @Override
    public void onLogoutFailed(String error) {
        Log.e(TAG, "error while logging out: " + error);
        Utils.wipeUserData();
        invalidateOptionsMenu();
    }
    
    @Override
    public void onSignupSuccessful(String userId) {
        //do nothing
    }

    @Override
    public void onSignupFailed(String error) {
        Log.e(TAG, "error while signin up: " + error);
    }

}
