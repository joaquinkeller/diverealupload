/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.activities;

import java.io.File;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.R;
import com.hybridearth.coordinaterecorder.objects.MapMakerMarker;
import com.hybridearth.coordinaterecorder.objects.MapOverlay;
import com.hybridearth.coordinaterecorder.objects.MetaioMarker;
import com.hybridearth.coordinaterecorder.server.DownloadManager;
import com.hybridearth.coordinaterecorder.utils.Log;
import com.hybridearth.coordinaterecorder.utils.Utils;
import com.hybridearth.coordinaterecorder.utils.Utils.Task;

/**
 * 
 * @author sciarcia
 * 
 */
public class MarkersActivity extends BaseActivity implements OnMarkerClickListener, OnMapLongClickListener, OnMarkerDragListener {

    private static final String TAG            = "MarkersActivity";

    private EditText            mAltEditText   = null;
    private EditText            mWidthEditText = null;
    private EditText            mIdEditText    = null;
    private CheckBox            mPublic        = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!MapMakerApplication.getInstance().isConnected()) {
            new AlertDialog.Builder(this).setTitle(getString(R.string.connection_error)).setMessage(getString(R.string.need_internet))
                    .setPositiveButton(getString(R.string.settings), new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    }).setNegativeButton(android.R.string.cancel, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                        }
                    }).create().show();
        } else {
            if (initMap()) {
                mMap.setOnMarkerClickListener(this);
                mMap.setOnMarkerDragListener(this);

                new DownloadManager<MapOverlay>(this).execute(Task.DOWNLOAD_OVERLAYS);
                new DownloadManager<MetaioMarker>(this).execute(Task.DOWNLOAD_MARKERS);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (MapMakerApplication.getAppState()) {
            case EDIT:
                getMenuInflater().inflate(R.menu.idmarkers, menu);
                if (mMap != null) {
                    mMap.setOnMapLongClickListener(this);
                }
                break;
            case LOGGED_IN:
                getMenuInflater().inflate(R.menu.no_edit, menu);
                if (mMap != null) {
                    mMap.setOnMapLongClickListener(null);
                }
                break;
            default:
                // do nothing
                Log.d(TAG, Consts.UNKNOWN_OPTION);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_upload_markers:
                onClickUpload(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public boolean onMarkerClick(Marker marker) {
        showMarkerDialog(marker);
        return true;
    }

    @Override
    public void onImageDowloaded(MapMakerMarker marker, String imgPath) {
        mSpinner.setVisibility(View.GONE);
        mPicImageView.setVisibility(View.VISIBLE);
        Utils.loadBitmap(imgPath, mPicImageView);
    }
    
    /**
     * when clicking on a marker on the map, opens up a dialog to show/edit the
     * details, depending on the appState
     * 
     * @param marker
     *            the clicked marker
     */
    protected void showMarkerDialog(final Marker marker) {
        final MetaioMarker mark = (MetaioMarker) mMarkersTranslationMap.get(marker);
        mSelectedMarker = mark;
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        switch (MapMakerApplication.getAppState()) {
            case EDIT:
                alert.setTitle(getString(R.string.edit_id_marker));
                break;
            case LOGGED_IN:
            default:
                alert.setTitle(getString(R.string.id_marker_details));
                break;
        }
        double lat = (double) Math.round(marker.getPosition().latitude * 1000000) / 1000000;
        double lon = (double) Math.round(marker.getPosition().longitude * 1000000) / 1000000;
        alert.setMessage("ID: " + mark.getId() + "\n" + getString(R.string.position) + " [" + lat + ", " + lon + "]");

        LayoutInflater inflater = MapMakerApplication.getInflater();

        View v = inflater.inflate(R.layout.dialog_view_id_marker, null, false);
        alert.setView(v);

        final ImageView pic = (ImageView) v.findViewById(R.id.picture_preview);
        mPicImageView = pic;
        mSpinner = (ProgressBar)v.findViewById(R.id.waiting_image);
        String picPath = mark.getPicPath();
        if (picPath != null && !"".equals(picPath)) {
            if(!(new File(picPath)).exists()) {
                mSpinner.setVisibility(View.VISIBLE);
                mPicImageView.setVisibility(View.GONE);
                new DownloadManager<MapOverlay>(this, mark).execute(Task.DOWNLOAD_IMAGE);
            } else {
                Utils.loadBitmap(picPath, mPicImageView);
            }
        }
        Button associatePanoButton = (Button) v.findViewById(R.id.associate_button);
        associatePanoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAddPicture();
            }
        });
        if (!mark.canIEdit()) {
            associatePanoButton.setVisibility(View.GONE);
        }

        mWidthEditText = (EditText) v.findViewById(R.id.edit_text_height);
        mWidthEditText.setText(String.valueOf(mark.getWidth()));
        mAltEditText = (EditText) v.findViewById(R.id.edit_text_alt);
        mAltEditText.setText(String.valueOf(mark.getAltitude()));

        mIdEditText = (EditText) v.findViewById(R.id.edit_text_metaioid);
        mIdEditText.setText(String.valueOf(mark.getMetaioID()));
        if (!mark.canIEdit()) {
            mWidthEditText.setEnabled(false);
            mAltEditText.setEnabled(false);
            mIdEditText.setEnabled(false);
        }

        TextView owner = (TextView) v.findViewById(R.id.owner);
        owner.setText(getString(R.string.owner) + mark.getOwner());

        mPublic = (CheckBox) v.findViewById(R.id.makeMarkerPublic);
        mPublic.setChecked(mark.isEditable());
        if (mark.canIEdit()) {
            mPublic.setEnabled(true);
            mPublic.setVisibility(View.VISIBLE);
            alert.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    int altitude = Integer.valueOf(mAltEditText.getText().toString());
                    int width = Integer.valueOf(mWidthEditText.getText().toString());
                    int id = Integer.valueOf(mIdEditText.getText().toString());
                    boolean editable = mPublic.isChecked();
                    if(altitude != mark.getAltitude() ||
                            width != mark.getWidth() ||
                            id != mark.getMetaioID() ||
                            editable != mark.isEditable()) {
                        mark.setAltitude(altitude);
                        mark.setWidth(width);
                        mark.setEditable(editable);
                        mark.setMetaioID(id);
                        if(mAddedMarkers.contains(mark)){
                           //we just created this, do nothing 
                        } else if(!mModifiedMarkers.contains(mark)){
                            mModifiedMarkers.add(mark);
                        }
                    }
                    Log.d(TAG, "save marker");
                }
            });

            alert.setNeutralButton(R.string.delete, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if(mAddedMarkers.contains(mark)){
                        mAddedMarkers.remove(mark);
                    } else if(mModifiedMarkers.contains(mark)){
                        mModifiedMarkers.remove(mark);
                    } else {
                        mDeletedMarkers.add(mark);
                    }
                    mark.setDelete(true);
                    mark.setPicPath(null);
                    marker.remove();
                    Log.d(TAG, "remove marker");
                }
            });
            alert.setNegativeButton(R.string.edit_angle, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Log.d(TAG, "save marker - angle");
                    // Edit angle.
                    int altitude = Integer.valueOf(mAltEditText.getText().toString());
                    int width = Integer.valueOf(mWidthEditText.getText().toString());
                    int id = Integer.valueOf(mIdEditText.getText().toString());
                    boolean editable = mPublic.isChecked();
                    Log.d(TAG, "save metaio marker - angle");
                    editAngle(mMarkersTranslationMap.get(marker).getAngle());
                    enableAngle(mMarkersTranslationMap.get(marker));
                    if(altitude != mark.getAltitude() ||
                            width != mark.getWidth() ||
                            id != mark.getMetaioID() ||
                            editable != mark.isEditable()) {
                        mark.setAltitude(altitude);
                        mark.setWidth(width);
                        mark.setMetaioID(id);
                        mark.setEditable(editable);
                        if(!mModifiedMarkers.contains(mark)){
                            mModifiedMarkers.add(mark);
                        }
                    }
                    dialog.dismiss();
                }
            });
        } else {
            mPublic.setEnabled(false);
            mPublic.setVisibility(View.GONE);
        }
        alert.show();
    }

    @Override
    public void onMapLongClick(LatLng position) {
        Marker newMarker = mMap.addMarker(new MarkerOptions().position(position));
        newMarker.setDraggable(true);
        MetaioMarker newMetaioMarker = new MetaioMarker(Utils.nextSessionId(), position);
        newMetaioMarker.setAltitude(1800);
        newMetaioMarker.setUserData();
        mMarkersTranslationMap.put(newMarker, newMetaioMarker);
        mMarkers.add(newMetaioMarker);
        onMarkerClick(newMarker);
        mAddedMarkers.add(newMetaioMarker);
    }

    @Override
    public void onMarkersUploaded() {
        for(int i = mMarkers.size()-1;i >= 0;i--) {
            MetaioMarker m = mMarkers.get(i);
            if (m.isDelete()) {
                mMarkers.remove(i);
            }
        }
        resetChanges();
    }

}
