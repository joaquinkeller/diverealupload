/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.TextView;

import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.R;
import com.hybridearth.coordinaterecorder.utils.Utils;
import com.hybridearth.coordinaterecorder.views.CustomImageView;
import com.hybridearth.coordinaterecorder.views.CustomImageView.OnAngleChangeListener;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 * @author triponez
 * @author sciarcia
 */
public class FullscreenImageActivity extends Activity implements OnAngleChangeListener {

    private TextView            mPositionText;
    private double              angleOffset;
    private String              markerID;
    private Bitmap              fullImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        mPositionText = (TextView) findViewById(R.id.angle_text);

        Intent sender = getIntent();
        String panoPath = sender.getExtras().getString(Consts.PANO_PATH);
        markerID = sender.getExtras().getString(Consts.MARKER_ID);
        angleOffset = sender.getExtras().getDouble(Consts.ANGLE_OFFSET);

        mPositionText.setText(String.valueOf(angleOffset));

        CustomImageView panoImageView = (CustomImageView) findViewById(R.id.imageview);

        panoImageView.setOnAngleChangeListener(this);
        panoImageView.setAngle(angleOffset);
        
        Utils.loadBitmap(panoPath, panoImageView);
    }

    /**
     * Gathers the new angle set on the panorama and sends it back to the calling activity
     * 
     * @param v the 'validate' button used to end the action
     */
    public void onClickValidate(View v) {
        if (fullImage != null) {
            fullImage.recycle();
        }
        fullImage = null;
        Intent result = new Intent();

        result.putExtra(Consts.MARKER_ID, markerID);
        result.putExtra(Consts.ANGLE_OFFSET, angleOffset);

        if (getParent() == null) {
            setResult(Activity.RESULT_OK, result);
        } else {
            getParent().setResult(Activity.RESULT_OK, result);
        }

        finish();
    }

    @Override
    public void onPositionChange(CustomImageView view, double newAngle) {
        angleOffset = newAngle;
        mPositionText.setText(String.valueOf(newAngle));
    }

}
