/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 Orange
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 */
package com.hybridearth.coordinaterecorder.activities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Pair;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hybridearth.coordinaterecorder.Consts;
import com.hybridearth.coordinaterecorder.MapMakerApplication;
import com.hybridearth.coordinaterecorder.R;
import com.hybridearth.coordinaterecorder.objects.MapMakerMarker;
import com.hybridearth.coordinaterecorder.objects.MapOverlay;
import com.hybridearth.coordinaterecorder.objects.PanoramaMarker;
import com.hybridearth.coordinaterecorder.objects.PanoramaMarker.Type;
import com.hybridearth.coordinaterecorder.server.DownloadManager;
import com.hybridearth.coordinaterecorder.utils.Log;
import com.hybridearth.coordinaterecorder.utils.Utils;
import com.hybridearth.coordinaterecorder.utils.Utils.Task;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * 
 * @author sciarcia
 * 
 */
public class PanosActivity extends BaseActivity implements OnMarkerClickListener, OnMapLongClickListener, OnMarkerDragListener {

    private static final String             TAG            = "PanosActivity";

    private boolean                         mAddingLine    = false;
    /** Special case to handle the addition/editing/removal of links between panoramas */
    private AddNeighborsMarkerClickListener mAddNeighborsMarkerClickListener;

    /** list of lines connecting panoramas. It represents the 'neighboring' information visually */
    private List<Polyline>                  mLinks         = new ArrayList<Polyline>();

    private EditText                        mAltEditText   = null;
    private EditText                        mPitchEditText = null;
    private CheckBox                        mPublic        = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!MapMakerApplication.getInstance().isConnected()) {
            new AlertDialog.Builder(this).setTitle(getString(R.string.connection_error)).setMessage(getString(R.string.need_internet))
                    .setPositiveButton(getString(R.string.settings), new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    }).setNegativeButton(android.R.string.cancel, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                        }
                    }).create().show();
        } else if (initMap()) {
            mMap.setOnMarkerClickListener(this);
            mMap.setOnMapLongClickListener(this);
            mMap.setOnMarkerDragListener(this);

            new DownloadManager<MapOverlay>(this).execute(Task.DOWNLOAD_OVERLAYS);
            new DownloadManager<PanoramaMarker>(this).execute(Task.DOWNLOAD_PANOS);

            mMapImageView = (ImageView) findViewById(R.id.map);
            mButtonsLayout = (LinearLayout) findViewById(R.id.buttons_layout);
        }
        setupViews();
    }

    /**
     * finds views and sets up listeners on them
     */
    private void setupViews() {
        Button cancel = (Button) findViewById(R.id.cancelButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickCancel();
            }
        });
        Button done = (Button) findViewById(R.id.doneButton);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDone();
            }
        });
    }

    @Override
    public void onBackPressed() {
        switch (MapMakerApplication.getAppState()) {
            case EDIT:
                if (mEditing || mEditingImg) {
                    onClickCancel();
                } else {
                    super.onBackPressed();
                }
                break;
            default:
                super.onBackPressed();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (MapMakerApplication.getAppState()) {
            case EDIT:
                getMenuInflater().inflate(R.menu.panoramas, menu);
                if (mMap != null) {
                    mMap.setOnMapLongClickListener(this);
                }
                break;
            case LOGGED_IN:
                getMenuInflater().inflate(R.menu.no_edit, menu);
                if (mMap != null) {
                    mMap.setOnMapLongClickListener(null);
                }
                break;
            default:
                // do nothing
                Log.d(TAG, Consts.UNKNOWN_OPTION);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                onClickAddMap();
                return true;
            case R.id.action_edit_maps:
                onClickEditMap();
                return true;
            case R.id.action_polyline:
                onClickAddLine();
                return true;
            case R.id.action_upload_panos:
                onClickUpload(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * "add overlay" menu entry selected.
     * The user wants to add an overlay to the map, we ask him if he wants to take a picture or select an image from gallery
     */
    private void onClickAddMap() {
        final CharSequence[] items = { getString(R.string.take_photo), getString(R.string.gallery) };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.select).setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    Utils.startCamera(PanosActivity.this, mImageUri);
                    dialog.dismiss();
                } else if (items[item].equals(getString(R.string.gallery))) {
                    Utils.startGallery(PanosActivity.this);
                    dialog.dismiss();
                }
            }
        }).create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent returnedIntent) {
        super.onActivityResult(requestCode, resultCode, returnedIntent);
        switch (requestCode) {
            case Consts.CROP_PIC_REQUEST:
                if (resultCode == RESULT_OK) {
                    String path = returnedIntent.getStringExtra(CropImage.IMAGE_PATH);
                    // if something received
                    if (path != null) {
                        Log.d(TAG, mImageUri.toString());
                        imgPath = path;
                        runMapEditor();
                    }
                }
                break;
            default:
                // do nothing
                Log.d(TAG, Consts.UNKNOWN_OPTION);
                break;
        }
    }

    /** The {@link ImageView} where the custom map is put */
    private ImageView    mMapImageView;
    private LinearLayout mButtonsLayout;
    /** Allows the user to change image opacity */
    private SeekBar      mSeekBar;
    /** flag to indicate that we're adding a ground overlay to the map */
    private boolean      mEditing    = false;
    /** flag to indicate that we're position the image representing the overlay on the map */
    private boolean      mEditingImg = false;

    /** 
     * Start the map editor once a picture was chosen
     * This includes two buttons at the bottom (cancel/done), a seekbar to modify opacity of the image we're placing 
     * and the imageview itself
     */
    private void runMapEditor() {
        if (mMapImageView != null) {
            mEditingImg = true;
            mMapImageView.setVisibility(View.VISIBLE);
            Utils.loadBitmap(imgPath, mMapImageView);

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;
            int screenHeight = size.y;
            int height = mMapImageView.getLayoutParams().height;
            int width = mMapImageView.getLayoutParams().width;

            if (height > width) {
                mMapImageView.getLayoutParams().height = screenHeight - 100;
                mMapImageView.getLayoutParams().width = (width * (screenHeight - 100)) / height;
            } else {
                mMapImageView.getLayoutParams().width = screenWidth - 100;
                mMapImageView.getLayoutParams().height = (height * (screenWidth - 200)) / width;
            }
            mSeekBar = (SeekBar) findViewById(R.id.slider);
            if (mSeekBar != null) {
                mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        float value = (float) (progress / 100.0);
                        mMapImageView.setAlpha(value);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // do nothing
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // do nothing
                    }
                });
                mSeekBar.setVisibility(View.VISIBLE);
            }
        }
        if (mButtonsLayout != null) {
            mButtonsLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * "edit overlay" menu entry selected.
     * we show yellow markers to allow the user to select which overlay he wants to modify, 
     * hiding all others panoramas
     */
    private void onClickEditMap() {
        mEditing = true;
        for (Marker m : mOverlaysTranslationMap.keySet()) {
            m.setVisible(true);
        }
        clearLines();
        for (Marker m : mMarkersTranslationMap.keySet()) {
            m.setVisible(false);
        }
    }

    /**
     * Called when the user pressed the Cancel {@link Button}
     * If we're editing links between panoramas we discard any change the user made and go back to previous state.
     * Otherwise it means we're adding/editing an overlay. As before we go back to the previous state discarding changes
     */
    private void onClickCancel() {
        if (mAddingLine) {
            mAddingLine = false;
            ((LinearLayout) findViewById(R.id.buttons_layout)).setVisibility(View.GONE);
            for (Polyline pl : mAddNeighborsMarkerClickListener.getLinks()) {
                pl.remove();
            }
            mMap.setOnMarkerClickListener(this);
            mMap.setOnMapLongClickListener(this);
        } else {
            if (mMapImageView != null) {
                mMapImageView.setImageDrawable(null);
                mMapImageView.setVisibility(View.GONE);
            }
            if (mButtonsLayout != null) {
                mButtonsLayout.setVisibility(View.GONE);
            }
            if (mSeekBar != null) {
                mSeekBar.setVisibility(View.GONE);
            }
            if (mEditing) {
                // re-show overlays
                for (Marker m : mOverlaysTranslationMap.keySet()) {
                    m.setVisible(false);
                    mOverlaysTranslationMap.get(m).first.setVisible(true);
                }
                // re-show markers
                for (Marker m : mMarkersTranslationMap.keySet()) {
                    m.setVisible(true);
                }
                // re-show polylines
                drawConnectionLines();
                mMap.setOnMapLongClickListener(this);
            }
            mEditingImg = false;
            mEditing = false;
        }
    }

    /**
     * Called when the user pressed the Done {@link Button}
     * If we're editing links between panoramas we add the new links to the 'official' list and restore previous state
     * Otherwise we save the new data for the modified overlay or the added overlay and add it to the overlays list
     */
    private void onClickDone() {
        if (mAddingLine) {
            // Here I'm adding links between panoramas
            mLinks.addAll(mAddNeighborsMarkerClickListener.getLinks());
            mAddingLine = false;
            ((LinearLayout) findViewById(R.id.buttons_layout)).setVisibility(View.GONE);
            mMap.setOnMarkerClickListener(this);
            mMap.setOnMapLongClickListener(this);
        } else {
            // Here I'm adding/editing a map overlay
            // Map position
            View view = (View) findViewById(R.id.googlemap);
            Point viewPosition = new Point(Utils.getRelativeLeft(view), Utils.getRelativeTop(view));

            // height of imageView
            double imageViewHeight = mMapImageView.getMeasuredHeight();
            // width of imageView
            double imageViewWidth = mMapImageView.getMeasuredWidth();
            // original height of underlying image
            double imageOriginalHeight = mMapImageView.getDrawable().getIntrinsicHeight();
            // original width of underlying image
            double imageOriginalWidth = mMapImageView.getDrawable().getIntrinsicWidth();

            double imageRealWidth = imageViewWidth;
            double imageRealHeight = imageViewHeight;

            if (imageViewHeight / imageOriginalHeight <= imageViewWidth / imageOriginalWidth) {
                // rescaled width of image within ImageView
                imageRealWidth = imageOriginalWidth * imageViewHeight / imageOriginalHeight;
            } else {
                // rescaled height of image within ImageView
                imageRealHeight = imageOriginalHeight * imageViewWidth / imageOriginalWidth;
            }

            // Top left position = NorthWest
            Point northWestPoint = new Point(Utils.getRelativeLeft(mMapImageView) - viewPosition.x + (int) (imageViewWidth - imageRealWidth) / 2,
                    Utils.getRelativeTop(mMapImageView) - viewPosition.y + (int) (imageViewHeight - imageRealHeight) / 2);

            // Top right position = NorthEast
            Point northEastPoint = new Point(Utils.getRelativeLeft(mMapImageView) - viewPosition.x + mMapImageView.getWidth() - (int) (imageViewWidth - imageRealWidth) / 2,
                    northWestPoint.y);

            // Bottom Left position = SouthWest
            Point southWestPoint = new Point(northWestPoint.x, northWestPoint.y + (int) imageRealHeight);

            // Bottom Right position = SouthWest
            Point southEastPoint = new Point(northEastPoint.x, southWestPoint.y);

            Point anchor = new Point((northEastPoint.x + southWestPoint.x) / 2, (northEastPoint.y + southWestPoint.y) / 2);

            Projection proj = mMap.getProjection();
            LatLng northWestLatLng = proj.fromScreenLocation(northWestPoint);
            LatLng northEastLatLng = proj.fromScreenLocation(northEastPoint);
            LatLng southEastLatLng = proj.fromScreenLocation(southEastPoint);
            LatLng anchorLatLng = proj.fromScreenLocation(anchor);

            float width = Utils.distanceBetween(northEastLatLng, northWestLatLng);
            float height = Utils.distanceBetween(northEastLatLng, southEastLatLng);

            if (mCurrentOverlayMarker != null) {
                //editing existing overlay
                Pair<GroundOverlay, MapOverlay> oldPair = mOverlaysTranslationMap.get(mCurrentOverlayMarker);
                oldPair.first.setBearing(mMap.getCameraPosition().bearing);
                oldPair.first.setPosition(anchorLatLng);
                oldPair.first.setDimensions(width, height);
                oldPair.second.setPosition(anchorLatLng);
                oldPair.second.setBearing(mMap.getCameraPosition().bearing);
                oldPair.second.setWidth(width);
                oldPair.second.setHeight(height);
                mCurrentOverlayMarker.setPosition(anchorLatLng);
                if(!mAddedOverlays.contains(oldPair.second)){
                    mModifiedOverlays.add(oldPair.second);
                }
            } else {
                //create new overlay
                BitmapDescriptor bd = BitmapDescriptorFactory.fromPath(imgPath);
                GroundOverlay go = mMap.addGroundOverlay(new GroundOverlayOptions().image(bd).anchor(0.5f, 0.5f).bearing(mMap.getCameraPosition().bearing)
                        .position(anchorLatLng, width, height));
                MapOverlay mo = new MapOverlay(anchorLatLng, width, height, imgPath, mMap.getCameraPosition().bearing);
                mo.setChangedPicture(true);
                mAddedOverlays.add(mo);
                Marker key = mMap.addMarker(new MarkerOptions().position(anchorLatLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
                key.setVisible(false);
                mOverlaysTranslationMap.put(key, Pair.create(go, mo));
            }
            onClickCancel();
        }
    }

    @Override
    public void onMarkersUploaded() {
        for (int i = mPanos.size() - 1; i >= 0; i--) {
            PanoramaMarker p = mPanos.get(i);
            if (p.isDelete()) {
                mPanos.remove(i);
            }
        }
        resetChanges();
    }

    /**
     * "edit links" menu entry selected.
     * change markerclicklistener to accomodate this state of things
     */
    private void onClickAddLine() {
        mAddingLine = true;
        mAddNeighborsMarkerClickListener = new AddNeighborsMarkerClickListener();
        mMap.setOnMarkerClickListener(mAddNeighborsMarkerClickListener);
        mMap.setOnMapLongClickListener(null);
        ((LinearLayout) findViewById(R.id.buttons_layout)).setVisibility(View.VISIBLE);
    }

    /** currently selected overlay to modify/remove */
    private Marker mCurrentOverlayMarker;

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (!mEditing) {
            showMarkerDialog(marker);
        } else {
            mCurrentOverlayMarker = marker;
            // here i clicked on an overlay marker, ask the user if he wants to
            // delete it or modify it
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.remove_or_modify)).setPositiveButton(getString(R.string.remove), new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int arg1) {
                    Pair<GroundOverlay, MapOverlay> p = mOverlaysTranslationMap.get(mCurrentOverlayMarker);
                    p.first.remove();
                    p.second.setDelete(true);
                    if(mAddedOverlays.contains(p.second)){
                        mAddedOverlays.remove(p.second);
                    } else if(mModifiedOverlays.contains(p.second)){
                        mModifiedOverlays.remove(p.second);
                        mDeletedOverlays.add(p.second);
                    } else {
                        mDeletedOverlays.add(p.second);
                    }
                    mCurrentOverlayMarker.remove();
                    dialog.dismiss();
                }
            }).setNegativeButton(R.string.modify, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int arg1) {
                    // hide all overlays
                    for (Marker m : mOverlaysTranslationMap.keySet()) {
                        m.setVisible(false);
                        mOverlaysTranslationMap.get(m).first.setVisible(false);
                    }
                    mMap.setOnMapLongClickListener(null);
                    imgPath = mOverlaysTranslationMap.get(mCurrentOverlayMarker).second.getPicPath();
                    runMapEditor();
                    dialog.dismiss();
                }
            }).create().show();
        }
        return true;
    }

    private String imgPath = null;

    @Override
    public void onImageDowloaded(MapMakerMarker marker, String imgPath) {
        mSpinner.setVisibility(View.GONE);
        mPicImageView.setVisibility(View.VISIBLE);
        Utils.loadBitmap(imgPath, mPicImageView);
    }

    /**
     * when clicking on a marker on the map, opens up a dialog to show/edit the
     * details, depending on the appState
     * 
     * @param marker
     *            the clicked marker
     */
    private void showMarkerDialog(final Marker marker) {
        final PanoramaMarker mark = (PanoramaMarker) mMarkersTranslationMap.get(marker);
        mSelectedMarker = mark;
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        switch (MapMakerApplication.getAppState()) {
            case EDIT:
                alert.setTitle(getString(R.string.edit_panorama));
                break;
            case LOGGED_IN:
            default:
                alert.setTitle(getString(R.string.panorama_details));
                break;
        }
        double lat = (double) Math.round(marker.getPosition().latitude * 1000000) / 1000000;
        double lon = (double) Math.round(marker.getPosition().longitude * 1000000) / 1000000;
        alert.setMessage("ID: " + mark.getId() + "\n" + getString(R.string.position) + " [" + lat + ", " + lon + "]");

        LayoutInflater inflater = MapMakerApplication.getInflater();

        final View v = inflater.inflate(R.layout.dialog_view_panorama_marker, null, false);
        alert.setView(v);

        final ImageView pic = (ImageView) v.findViewById(R.id.picture_preview);
        mPicImageView = pic;
        mSpinner = (ProgressBar) v.findViewById(R.id.waiting_image);
        String picPath = mark.getPicPath();
        if (picPath != null && !"".equals(picPath)) {
            if (!(new File(picPath)).exists()) {
                mSpinner.setVisibility(View.VISIBLE);
                mPicImageView.setVisibility(View.GONE);
                new DownloadManager<MapOverlay>(this, mark).execute(Task.DOWNLOAD_IMAGE);
            } else {
                Utils.loadBitmap(picPath, mPicImageView);
            }
        }
        Button associatePanoButton = (Button) v.findViewById(R.id.associate_button);
        associatePanoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAddPicture();
            }
        });
        if (!mark.canIEdit()) {
            associatePanoButton.setVisibility(View.GONE);
        }

        final Button changeStatusButton = (Button) v.findViewById(R.id.status_button);
        changeStatusButton.setText(mark.getType().toString());
        changeStatusButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mark.getType() == Type.INNER) {
                    mark.setType(Type.ENTRANCE);
                    marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else {
                    mark.setType(Type.INNER);
                    marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
                }
                changeStatusButton.setText(mark.getType().toString());
            }
        });
        if (!mark.canIEdit()) {
            changeStatusButton.setClickable(false);
        }
        mPanoAngleOffset = (Button) v.findViewById(R.id.angle_offset_button);
        mPanoAngleOffset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fullScreenPano = new Intent(PanosActivity.this, FullscreenImageActivity.class);
                fullScreenPano.putExtra(Consts.PANO_PATH, mark.getPicPath());
                fullScreenPano.putExtra(Consts.MARKER_ID, mark.getId());
                fullScreenPano.putExtra(Consts.ANGLE_OFFSET, ((PanoramaMarker) mark).getPanoCenter());
                startActivityForResult(fullScreenPano, Consts.PANO_OFFSET_REQUEST);
            }
        });
        if (!mark.canIEdit()) {
            mPanoAngleOffset.setVisibility(View.GONE);
        } else if (picPath == null || "".equals(picPath)) {
            mPanoAngleOffset.setVisibility(View.GONE);
        }
        mAltEditText = (EditText) v.findViewById(R.id.edit_text_camera_alt);
        mAltEditText.setText(String.valueOf(mark.getCameraAlt()));

        mPitchEditText = (EditText) v.findViewById(R.id.edit_text_pitch);
        mPitchEditText.setText(String.valueOf(mark.getPitch()));
        if (!mark.canIEdit()) {
            mAltEditText.setEnabled(false);
            mPitchEditText.setEnabled(false);
        }

        TextView owner = (TextView) v.findViewById(R.id.owner);
        owner.setText(getString(R.string.owner) + mark.getOwner());

        mPublic = (CheckBox) v.findViewById(R.id.makePanoPublic);
        mPublic.setChecked(mark.isEditable());
        if (mark.canIEdit()) {
            mPublic.setEnabled(true);
            mPublic.setVisibility(View.VISIBLE);
            alert.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    double cameraAlt = Double.valueOf(mAltEditText.getText().toString());
                    double pitch = Double.valueOf(mPitchEditText.getText().toString());
                    boolean editable = mPublic.isChecked();
                    if (cameraAlt != mark.getCameraAlt() || pitch != mark.getPitch() || editable != mark.isEditable()) {
                        mark.setCameraAlt(cameraAlt);
                        mark.setPitch(pitch);
                        mark.setEditable(editable);
                        if (mAddedPanoramas.contains(mark)) {
                            // we just created this, do nothing
                        } else if (!mModifiedPanoramas.contains(mark)) {
                            mModifiedPanoramas.add(mark);
                        }
                    }
                    Log.d(TAG, "save panorama");
                }
            });
            alert.setNeutralButton(R.string.delete, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    updateNeighbors(marker, true);
                    if (mAddedPanoramas.contains(mark)) {
                        mAddedPanoramas.remove(mark);
                    } else if (mModifiedPanoramas.contains(mark)) {
                        mModifiedPanoramas.remove(mark);
                    } else {
                        mDeletedPanoramas.add(mark);
                    }
                    mark.setDelete(true);
                    mark.setPicPath(null);
                    marker.remove();
                    drawConnectionLines();
                    Log.d(TAG, "remove panorama");
                }
            });
            alert.setNegativeButton(R.string.edit_angle, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Log.d(TAG, "save panorama - angle");
                    // Edit angle.
                    double cameraAlt = Double.valueOf(mAltEditText.getText().toString());
                    double pitch = Double.valueOf(mPitchEditText.getText().toString());
                    boolean editable = mPublic.isChecked();
                    editAngle(mark.getAngle());
                    enableAngle(mark);
                    if (cameraAlt != mark.getCameraAlt() || pitch != mark.getPitch() || editable != mark.isEditable()) {
                        mark.setCameraAlt(cameraAlt);
                        mark.setPitch(pitch);
                        mark.setEditable(editable);
                        if (!mModifiedPanoramas.contains(mark)) {
                            mModifiedPanoramas.add(mark);
                        }
                    }
                    dialog.dismiss();
                }
            });
        } else {
            mPublic.setEnabled(false);
            mPublic.setVisibility(View.GONE);
        }
        alert.show();
    }

    @Override
    public void onMarkerDragEnd(Marker draggedMarker) {
        super.onMarkerDragEnd(draggedMarker);
        updateNeighbors(draggedMarker, false);
        drawConnectionLines();
    }

    private void updateNeighbors(Marker mark, boolean delete) {
        PanoramaMarker m = (PanoramaMarker) mMarkersTranslationMap.get(mark);
        if (!delete) {
            m.setPosition(mark.getPosition());
            mMarkersTranslationMap.put(mark, m);
            for (PanoramaMarker pm : mPanos) {
                for (PanoramaMarker n : pm.getNeighbors()) {
                    if (n.getId().equals(m.getId())) {
                        n.setPosition(m.getPosition());
                        if(mAddedPanoramas.contains(n)){
                            //we just created this, do nothing
                        } else if(!mModifiedPanoramas.contains(n)){
                            //here we're moving a panorama in our neighbors list.
                            //this means we're being modified
                            mModifiedPanoramas.add(n);
                        }
                    }
                }
            }
        } else {
            for (PanoramaMarker pm : mPanos) {
                for (int i = pm.getNeighbors().size() - 1; i >= 0; i--) {
                    if (pm.getNeighbors().get(i).getId().equals(m.getId())) {
                        pm.getNeighbors().remove(i);
                        if(mAddedPanoramas.contains(pm)){
                            //we just created this, do nothing
                        } else if(!mModifiedPanoramas.contains(pm)){
                            //here we're deleting a panorama from our neighbors list.
                            //this means we're being modified
                            mModifiedPanoramas.add(pm);
                        }
                    }
                }
            }
            mMarkersTranslationMap.remove(mark);
        }
    }

    @Override
    public void onMarkerDragStart(Marker draggedMarker) {
        clearLines();
    }

    @Override
    public void onMapLongClick(LatLng position) {
        Marker newMarker = mMap.addMarker(new MarkerOptions().position(position).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
        newMarker.setDraggable(true);
        PanoramaMarker newPanoMarker = new PanoramaMarker(Utils.nextSessionId(), position);
        newPanoMarker.setUserData();
        newPanoMarker.setCameraAlt(1600);
        mMarkersTranslationMap.put(newMarker, newPanoMarker);
        mPanos.add(newPanoMarker);
        onMarkerClick(newMarker);
        mAddedPanoramas.add(newPanoMarker);
    }

    @Override
    public void onPanoramaMarkersDownloaded(List<PanoramaMarker> markers) {
        super.onPanoramaMarkersDownloaded(markers);
        if (mPanos != null) {
            drawConnectionLines();
        }
    }

    private void clearLines() {
        for (Polyline l : mLinks) {
            l.remove();
        }
        mLinks.clear();
    }

    private void drawConnectionLines() {
        clearLines();
        for (PanoramaMarker m : mPanos) {
            if (m.isDelete()) {
                continue;
            }
            Log.d(TAG, "drawLines: " + m.printNeighborsList());
            List<PanoramaMarker> neighbors = m.getNeighbors();
            for (PanoramaMarker n : neighbors) {
                if (n.isDelete()) {
                    continue;
                }
                if (m.getId().compareTo(n.getId()) < 0) {
                    Log.d(TAG, "~~ draw line from " + m.getId() + " to " + n.getId());
                    mLinks.add(mMap.addPolyline(new PolylineOptions().width(5).color(Color.BLUE).add(m.getPosition()).add(n.getPosition())));
                }
            }
        }
    }

    private class AddNeighborsMarkerClickListener implements OnMarkerClickListener {

        private static final String ADD_NEIGHBORS_TAG           = "AddNeighborsMarkerClickListener";

        private PanoramaMarker      startMarker    = null;
        private PanoramaMarker      endMarker      = null;
        private Marker              startMapMarker = null;
        private List<Polyline>      links          = new ArrayList<Polyline>();

        @Override
        public boolean onMarkerClick(Marker marker) {
            if (startMarker == null) {
                // clicking on start marker
                startMarker = (PanoramaMarker) mMarkersTranslationMap.get(marker);
                Log.d(ADD_NEIGHBORS_TAG, "START pano before: " + startMarker.printNeighborsList());
                startMapMarker = marker;
                startMapMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            } else if (marker.equals(startMapMarker)) {
                // re-clicking on start marker, abort
                if (((PanoramaMarker) startMarker).getType() == Type.INNER) {
                    startMapMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
                } else {
                    startMapMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                }
                startMarker = null;
            } else if (endMarker == null) {
                // clicking on end marker
                endMarker = (PanoramaMarker) mMarkersTranslationMap.get(marker);
                Log.d(ADD_NEIGHBORS_TAG, "END pano before: " + endMarker.printNeighborsList());
                if (startMarker.getType() == Type.INNER) {
                    startMapMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
                } else {
                    startMapMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                }
                if (startMarker.hasNeighbor(endMarker)) {
                    // remove neighbor at both ends
                    removePolyline(startMarker, endMarker);
                    startMarker.removeNeighbor(endMarker);
                    endMarker.removeNeighbor(startMarker);
                } else {
                    // add neighbor
                    links.add(mMap.addPolyline(new PolylineOptions().width(5).color(Color.BLUE).add(startMarker.getPosition(), endMarker.getPosition())));
                    startMarker.getNeighbors().add(endMarker);
                    endMarker.getNeighbors().add(startMarker);
                }
                Log.d(ADD_NEIGHBORS_TAG, "START pano after: " + startMarker.printNeighborsList());
                Log.d(ADD_NEIGHBORS_TAG, "END pano after: " + endMarker.printNeighborsList());
                Log.d(ADD_NEIGHBORS_TAG, "-------------");
                if (mAddedPanoramas.contains(startMarker)) {
                    // we just added this, do nothing
                } else if (!mModifiedPanoramas.contains(startMarker)) {
                    mModifiedPanoramas.add(startMarker);
                }
                if (mAddedPanoramas.contains(endMarker)) {
                    // we just added this, do nothing
                } else if (!mModifiedPanoramas.contains(endMarker)) {
                    mModifiedPanoramas.add(endMarker);
                }
                startMarker = null;
                endMarker = null;
            } else {
                // re-clicking on end marker, abort
                endMarker = null;
            }
            return true;
        }

        private void removePolyline(PanoramaMarker one, PanoramaMarker two) {
            for (Polyline l : links) {
                LatLng l0 = l.getPoints().get(0);
                LatLng l1 = l.getPoints().get(1);
                if ((Utils.equalsLatLng(l0, one.getPosition()) && Utils.equalsLatLng(l1, two.getPosition()))
                        || (Utils.equalsLatLng(l0, two.getPosition()) && Utils.equalsLatLng(l1, one.getPosition()))) {
                    links.remove(l);
                    l.remove();
                    break;
                }
            }
            // if i'm here it means that it's not a new polyline,
            // but it could be one between two markers that already existed
            for (Polyline l : mLinks) {
                LatLng l0 = l.getPoints().get(0);
                LatLng l1 = l.getPoints().get(1);
                if ((Utils.equalsLatLng(l0, one.getPosition()) && Utils.equalsLatLng(l1, two.getPosition()))
                        || (Utils.equalsLatLng(l0, two.getPosition()) && Utils.equalsLatLng(l1, one.getPosition()))) {
                    mLinks.remove(l);
                    l.remove();
                    break;
                }
            }
        }

        public List<Polyline> getLinks() {
            return links;
        }
    }
}
