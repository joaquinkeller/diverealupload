When walking in StreetView we almost have the feeling of being in the street. Imagine then to be able to interact with people physically in the street.
People wearing augmented reality glasses would see the avatars evolving in StreetView.

But what if StreetView hasn't yet covered your street? Or if you want to share your own private building?

Here's the solution: OpenStreetView.
With this application you can create your own StreetView-like panoramas and explore it sending your avatar in to take a look.

More info at [blog.hybridearth.net](http://blog.hybridearth.net).

## Dependencies: 
[CropImage](https://github.com/biokys/cropimage) and Google Play Services.
